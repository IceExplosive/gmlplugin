// This is a generated file. Not intended for manual editing.
package gmlplugin;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static gmlplugin.psi.GmlTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class GmlParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return gmlBase(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(ASSIGN_EXPRESSION, COMPUTE_EXPRESSION, CONDITIONAL_EXPRESSION, EXPRESSION,
      LITERAL_EXPRESSION, PLUS_MINUS_EXPRESSION, PLUS_MINUS_REV_EXPRESSION, REFERENCE_EXPRESSION,
      ROUND_BRACKET_EXPRESSION, STRUCT_DECLARATION, TEST_EXPRESSION),
  };

  /* ********************************************************** */
  // (lsbr (identifier | number | expression) rsbr)? (lrbr functionArgList? rrbr)? (dot (identifierRef | expression))*
  public static boolean afterIndentifier(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, AFTER_INDENTIFIER, "<after indentifier>");
    r = afterIndentifier_0(b, l + 1);
    r = r && afterIndentifier_1(b, l + 1);
    r = r && afterIndentifier_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (lsbr (identifier | number | expression) rsbr)?
  private static boolean afterIndentifier_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_0")) return false;
    afterIndentifier_0_0(b, l + 1);
    return true;
  }

  // lsbr (identifier | number | expression) rsbr
  private static boolean afterIndentifier_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LSBR);
    r = r && afterIndentifier_0_0_1(b, l + 1);
    r = r && consumeToken(b, RSBR);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier | number | expression
  private static boolean afterIndentifier_0_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_0_0_1")) return false;
    boolean r;
    r = consumeToken(b, IDENTIFIER);
    if (!r) r = consumeToken(b, NUMBER);
    if (!r) r = expression(b, l + 1, -1);
    return r;
  }

  // (lrbr functionArgList? rrbr)?
  private static boolean afterIndentifier_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_1")) return false;
    afterIndentifier_1_0(b, l + 1);
    return true;
  }

  // lrbr functionArgList? rrbr
  private static boolean afterIndentifier_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LRBR);
    r = r && afterIndentifier_1_0_1(b, l + 1);
    r = r && consumeToken(b, RRBR);
    exit_section_(b, m, null, r);
    return r;
  }

  // functionArgList?
  private static boolean afterIndentifier_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_1_0_1")) return false;
    functionArgList(b, l + 1);
    return true;
  }

  // (dot (identifierRef | expression))*
  private static boolean afterIndentifier_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!afterIndentifier_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "afterIndentifier_2", c)) break;
    }
    return true;
  }

  // dot (identifierRef | expression)
  private static boolean afterIndentifier_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, DOT);
    r = r && afterIndentifier_2_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifierRef | expression
  private static boolean afterIndentifier_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "afterIndentifier_2_0_1")) return false;
    boolean r;
    r = identifierRef(b, l + 1);
    if (!r) r = expression(b, l + 1, -1);
    return r;
  }

  /* ********************************************************** */
  // eq | noteq | multeq | diveq | pluseq | minuseq | compeq | andeq | modeq | oreq
  public static boolean assignOp(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignOp")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ASSIGN_OP, "<assign op>");
    r = consumeToken(b, EQ);
    if (!r) r = consumeToken(b, NOTEQ);
    if (!r) r = consumeToken(b, MULTEQ);
    if (!r) r = consumeToken(b, DIVEQ);
    if (!r) r = consumeToken(b, PLUSEQ);
    if (!r) r = consumeToken(b, MINUSEQ);
    if (!r) r = consumeToken(b, COMPEQ);
    if (!r) r = consumeToken(b, ANDEQ);
    if (!r) r = consumeToken(b, MODEQ);
    if (!r) r = consumeToken(b, OREQ);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // case expression colon statement? (break semicon)?
  public static boolean caseStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement")) return false;
    if (!nextTokenIs(b, CASE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, CASE);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, COLON);
    r = r && caseStatement_3(b, l + 1);
    r = r && caseStatement_4(b, l + 1);
    exit_section_(b, m, CASE_STATEMENT, r);
    return r;
  }

  // statement?
  private static boolean caseStatement_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement_3")) return false;
    statement(b, l + 1);
    return true;
  }

  // (break semicon)?
  private static boolean caseStatement_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement_4")) return false;
    caseStatement_4_0(b, l + 1);
    return true;
  }

  // break semicon
  private static boolean caseStatement_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement_4_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, BREAK, SEMICON);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // collision identifier
  public static boolean collision_event(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collision_event")) return false;
    if (!nextTokenIs(b, COLLISION)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COLLISION, IDENTIFIER);
    exit_section_(b, m, COLLISION_EVENT, r);
    return r;
  }

  /* ********************************************************** */
  // comment
  //     | enumDeclaration
  //     | constDeclaration
  //     | varDeclaration
  //     | globalvarDeclaration
  //     | statementBlock
  //     | ifStatement
  //     | withStatement
  //     | repeatStatement
  //     | doStatement
  //     | whileStatement
  //     | switchStatement
  //     | forStatement
  //     | returnStatement
  //     | expression
  public static boolean conlessStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "conlessStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONLESS_STATEMENT, "<conless statement>");
    r = consumeToken(b, COMMENT);
    if (!r) r = enumDeclaration(b, l + 1);
    if (!r) r = constDeclaration(b, l + 1);
    if (!r) r = varDeclaration(b, l + 1);
    if (!r) r = globalvarDeclaration(b, l + 1);
    if (!r) r = statementBlock(b, l + 1);
    if (!r) r = ifStatement(b, l + 1);
    if (!r) r = withStatement(b, l + 1);
    if (!r) r = repeatStatement(b, l + 1);
    if (!r) r = doStatement(b, l + 1);
    if (!r) r = whileStatement(b, l + 1);
    if (!r) r = switchStatement(b, l + 1);
    if (!r) r = forStatement(b, l + 1);
    if (!r) r = returnStatement(b, l + 1);
    if (!r) r = expression(b, l + 1, -1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // macro identifier literalExpression semicon?
  public static boolean constDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constDeclaration")) return false;
    if (!nextTokenIs(b, MACRO)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, MACRO, IDENTIFIER);
    r = r && literalExpression(b, l + 1);
    r = r && constDeclaration_3(b, l + 1);
    exit_section_(b, m, CONST_DECLARATION, r);
    return r;
  }

  // semicon?
  private static boolean constDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constDeclaration_3")) return false;
    consumeToken(b, SEMICON);
    return true;
  }

  /* ********************************************************** */
  // true | false | pi
  public static boolean defCosnt(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defCosnt")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, DEF_COSNT, "<def cosnt>");
    r = consumeToken(b, TRUE);
    if (!r) r = consumeToken(b, FALSE);
    if (!r) r = consumeToken(b, PI);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // default colon statement?
  public static boolean defaultStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defaultStatement")) return false;
    if (!nextTokenIs(b, DEFAULT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DEFAULT, COLON);
    r = r && defaultStatement_2(b, l + 1);
    exit_section_(b, m, DEFAULT_STATEMENT, r);
    return r;
  }

  // statement?
  private static boolean defaultStatement_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defaultStatement_2")) return false;
    statement(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // do lcbr statement rcbr until lrbr expression rrbr semicon?
  public static boolean doStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "doStatement")) return false;
    if (!nextTokenIs(b, DO)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DO, LCBR);
    r = r && statement(b, l + 1);
    r = r && consumeTokens(b, 0, RCBR, UNTIL, LRBR);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RRBR);
    r = r && doStatement_8(b, l + 1);
    exit_section_(b, m, DO_STATEMENT, r);
    return r;
  }

  // semicon?
  private static boolean doStatement_8(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "doStatement_8")) return false;
    consumeToken(b, SEMICON);
    return true;
  }

  /* ********************************************************** */
  // enum identifier lcbr (enumEntryDeclaration comma)* (enumEntryDeclaration comma?)? rcbr
  public static boolean enumDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration")) return false;
    if (!nextTokenIs(b, ENUM)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, ENUM, IDENTIFIER, LCBR);
    r = r && enumDeclaration_3(b, l + 1);
    r = r && enumDeclaration_4(b, l + 1);
    r = r && consumeToken(b, RCBR);
    exit_section_(b, m, ENUM_DECLARATION, r);
    return r;
  }

  // (enumEntryDeclaration comma)*
  private static boolean enumDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!enumDeclaration_3_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "enumDeclaration_3", c)) break;
    }
    return true;
  }

  // enumEntryDeclaration comma
  private static boolean enumDeclaration_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = enumEntryDeclaration(b, l + 1);
    r = r && consumeToken(b, COMMA);
    exit_section_(b, m, null, r);
    return r;
  }

  // (enumEntryDeclaration comma?)?
  private static boolean enumDeclaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration_4")) return false;
    enumDeclaration_4_0(b, l + 1);
    return true;
  }

  // enumEntryDeclaration comma?
  private static boolean enumDeclaration_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration_4_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = enumEntryDeclaration(b, l + 1);
    r = r && enumDeclaration_4_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // comma?
  private static boolean enumDeclaration_4_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumDeclaration_4_0_1")) return false;
    consumeToken(b, COMMA);
    return true;
  }

  /* ********************************************************** */
  // identifier (assignOp expression)?
  public static boolean enumEntryDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumEntryDeclaration")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    r = r && enumEntryDeclaration_1(b, l + 1);
    exit_section_(b, m, ENUM_ENTRY_DECLARATION, r);
    return r;
  }

  // (assignOp expression)?
  private static boolean enumEntryDeclaration_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumEntryDeclaration_1")) return false;
    enumEntryDeclaration_1_0(b, l + 1);
    return true;
  }

  // assignOp expression
  private static boolean enumEntryDeclaration_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enumEntryDeclaration_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assignOp(b, l + 1);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // event eventType lcbr statement* rcbr
  public static boolean eventDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eventDeclaration")) return false;
    if (!nextTokenIs(b, EVENT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVENT);
    r = r && eventType(b, l + 1);
    r = r && consumeToken(b, LCBR);
    r = r && eventDeclaration_3(b, l + 1);
    r = r && consumeToken(b, RCBR);
    exit_section_(b, m, EVENT_DECLARATION, r);
    return r;
  }

  // statement*
  private static boolean eventDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eventDeclaration_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "eventDeclaration_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // create | destroy | clean_up | step | step_begin | step_end | alarm_0 |alarm_1 | alarm_2 | alarm_3
  //     | alarm_4 | alarm_5 | alarm_6 | alarm_7 | alarm_8 | alarm_9 | alarm_10 | alarm_11 | draw | draw_GUI | draw_begin
  //     | draw_GUI_begin | draw_end | draw_GUI_end | pre_draw | post_draw | window_resize | mouse_left_down | mouse_right_down
  //     | mouse_middle_down | mouse_left_pressed | mouse_right_pressed | mouse_middle_pressed | mouse_left_release
  //     | mouse_right_release | mouse_middle_release | no_mouse | mouse_enter | mouse_leave | mouse_wheel_up | mouse_wheel_down
  //     | global_mouse_left_down | global_mouse_right_down | global_mouse_middle_down | global_mouse_left_release | global_mouse_right_release
  //     | global_mouse_middle_release | global_mouse_left_pressed | global_mouse_right_pressed | global_mouse_middle_pressed
  //     | key_left_released | key_left_down | key_left_pressed | key_up_released | key_up_down | key_up_pressed | key_right_released | key_right_down
  //     | key_right_pressed | key_down_released | key_down_down | key_down_pressed | key_control_released | key_control_down | key_control_pressed
  //     | key_alt_released | key_alt_down | key_alt_pressed | key_shift_released | key_shift_down | key_shift_pressed | key_space_released | key_space_down
  //     | key_space_pressed | key_enter_released | key_enter_down | key_enter_pressed | key_keypad_0_released | key_keypad_0_down | key_keypad_0_pressed
  //     | key_keypad_1_released | key_keypad_1_down | key_keypad_1_pressed | key_keypad_2_released | key_keypad_2_down | key_keypad_2_pressed
  //     | key_keypad_3_released | key_keypad_3_down | key_keypad_3_pressed | key_keypad_4_released | key_keypad_4_down | key_keypad_4_pressed
  //     | key_keypad_5_released | key_keypad_5_down | key_keypad_5_pressed | key_keypad_6_released | key_keypad_6_down | key_keypad_6_pressed
  //     | key_keypad_7_released | key_keypad_7_down | key_keypad_7_pressed | key_keypad_8_released | key_keypad_8_down | key_keypad_8_pressed
  //     | key_keypad_9_released | key_keypad_9_down | key_keypad_9_pressed | key_keypad_slash_released | key_keypad_slash_down | key_keypad_slash_pressed
  //     | key_keypad_mult_released | key_keypad_mult_down | key_keypad_mult_pressed | key_keypad_minus_released | key_keypad_minus_down | key_keypad_minus_pressed
  //     | key_keypad_plus_released | key_keypad_plus_down | key_keypad_plus_pressed | key_keypad_dot_released | key_keypad_dot_down | key_keypad_dot_pressed
  //     | key_0_released | key_0_down | key_0_pressed | key_1_released | key_1_down | key_1_pressed | key_2_down | key_2_pressed | key_3_up
  //     | key_3_down | key_3_pressed | key_4_released | key_4_down | key_4_pressed | key_5_released | key_5_down | key_5_pressed | key_6_up
  //     | key_6_down | key_6_pressed | key_7_released | key_7_down | key_7_pressed | key_8_released | key_8_down | key_8_pressed | key_9_up
  //     | key_9_down | key_9_pressed | key_A_released | key_A_down | key_A_pressed | key_B_released | key_B_down | key_B_pressed | key_C_up
  //     | key_C_down | key_C_pressed | key_D_released | key_D_down | key_D_pressed | key_E_released | key_E_down | key_E_pressed | key_F_up
  //     | key_F_down | key_F_pressed | key_G_released | key_G_down | key_G_pressed | key_H_released | key_H_down | key_H_pressed | key_I_up
  //     | key_I_down | key_I_pressed | key_J_released | key_J_down | key_J_pressed | key_K_released | key_K_down | key_K_pressed | key_L_up
  //     | key_L_down | key_L_pressed | key_M_released | key_M_down | key_M_pressed | key_N_released | key_N_down | key_N_pressed | key_O_up
  //     | key_O_down | key_O_pressed | key_P_released | key_P_down | key_P_pressed | key_Q_released | key_Q_down | key_Q_pressed | key_R_up
  //     | key_R_down | key_R_pressed | key_S_released | key_S_down | key_S_pressed | key_T_released | key_T_down | key_T_pressed | key_U_up
  //     | key_U_down | key_U_pressed | key_V_released | key_V_down | key_V_pressed | key_W_released | key_W_down | key_W_pressed | key_X_up
  //     | key_X_down | key_X_pressed | key_Y_released | key_Y_down | key_Y_pressed | key_Z_released | key_Z_down | key_Z_pressed | key_F1_up
  //     | key_F1_down | key_F1_pressed | key_F2_released | key_F2_down | key_F2_pressed | key_F3_released | key_F3_down | key_F3_pressed
  //     | key_F4_released | key_F4_down | key_F4_pressed | key_F5_released | key_F5_down | key_F5_pressed | key_F6_released | key_F6_down | key_F6_pressed
  //     | key_F7_released | key_F7_down | key_F7_pressed | key_F8_released | key_F8_down | key_F8_pressed | key_F9_released | key_F9_down | key_F9_pressed
  //     | key_F10_released | key_F10_down | key_F10_pressed | key_F11_released | key_F11_down | key_F11_pressed | key_F12_released | key_F12_down
  //     | key_F12_pressed | key_backspace_released | key_backspace_down | key_backspace_pressed | key_escape_released | key_escape_down | key_escape_pressed
  //     | key_home_released | key_home_down | key_home_pressed | key_end_released | key_end_down | key_end_pressed | key_page_up_released | key_page_up_down
  //     | key_page_up_pressed | key_page_down_released | key_page_down_down | key_page_down_pressed | key_delete_released | key_delete_down
  //     | key_delete_pressed | key_insert_released | key_insert_down | key_insert_pressed | key_no_released | key_no_down | key_no_pressed
  //     | key_any_released | key_any_down | key_any_pressed | gesture_tap | gesture_double_tap | gesture_drag_start | gesture_dragging
  //     | gesture_drag_end | gesture_flick | gesture_pinch_start | gesture_pinch_in | gesture_pinch_out | gesture_pinch_end
  //     | gesture_rotate_start | gesture_rotating | gesture_rotate_end | global_gesture_tap | global_gesture_double_tap
  //     | global_gesture_drag_start | global_gesture_dragging | global_gesture_drag_end | global_gesture_flick | global_gesture_pinch_start
  //     | global_gesture_pinch_in | global_gesture_pinch_out | global_gesture_pinch_end | global_gesture_rotate_start | global_gesture_rotating
  //     | global_gesture_rotate_end | outside_room | intersect_boundary | outside_view_0 | outside_view_1 | outside_view_2
  //     | outside_view_3 | outside_view_4 | outside_view_5 | outside_view_6 | outside_view_7 | intersect_view_0_boundary
  //     | intersect_view_1_boundary | intersect_view_2_boundary | intersect_view_3_boundary | intersect_view_4_boundary | intersect_view_5_boundary
  //     | intersect_view_6_boundary | intersect_view_7_boundary | game_start | game_end | room_start | room_end | animation_end
  //     | animation_update | animation_event | path_ended | user_event_0 | user_event_1 | user_event_2 | user_event_3 | user_event_4
  //     | user_event_5 | user_event_6 | user_event_7 | user_event_8 | user_event_9 | user_event_10 | user_event_11 | user_event_12
  //     | user_event_13 | user_event_14 | user_event_15 | async_audio_playback | async_audio_recording | async_cloud | async_dialog
  //     | async_http | async_in_app_purchase | async_image_loaded | async_networking | async_push_notification | async_save_load
  //     | async_social | async_steam | async_system
  //     | collision_event
  public static boolean eventType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eventType")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVENT_TYPE, "<event type>");
    r = consumeToken(b, CREATE);
    if (!r) r = consumeToken(b, DESTROY);
    if (!r) r = consumeToken(b, CLEAN_UP);
    if (!r) r = consumeToken(b, STEP);
    if (!r) r = consumeToken(b, STEP_BEGIN);
    if (!r) r = consumeToken(b, STEP_END);
    if (!r) r = consumeToken(b, ALARM_0);
    if (!r) r = consumeToken(b, ALARM_1);
    if (!r) r = consumeToken(b, ALARM_2);
    if (!r) r = consumeToken(b, ALARM_3);
    if (!r) r = consumeToken(b, ALARM_4);
    if (!r) r = consumeToken(b, ALARM_5);
    if (!r) r = consumeToken(b, ALARM_6);
    if (!r) r = consumeToken(b, ALARM_7);
    if (!r) r = consumeToken(b, ALARM_8);
    if (!r) r = consumeToken(b, ALARM_9);
    if (!r) r = consumeToken(b, ALARM_10);
    if (!r) r = consumeToken(b, ALARM_11);
    if (!r) r = consumeToken(b, DRAW);
    if (!r) r = consumeToken(b, DRAW_GUI);
    if (!r) r = consumeToken(b, DRAW_BEGIN);
    if (!r) r = consumeToken(b, DRAW_GUI_BEGIN);
    if (!r) r = consumeToken(b, DRAW_END);
    if (!r) r = consumeToken(b, DRAW_GUI_END);
    if (!r) r = consumeToken(b, PRE_DRAW);
    if (!r) r = consumeToken(b, POST_DRAW);
    if (!r) r = consumeToken(b, WINDOW_RESIZE);
    if (!r) r = consumeToken(b, MOUSE_LEFT_DOWN);
    if (!r) r = consumeToken(b, MOUSE_RIGHT_DOWN);
    if (!r) r = consumeToken(b, MOUSE_MIDDLE_DOWN);
    if (!r) r = consumeToken(b, MOUSE_LEFT_PRESSED);
    if (!r) r = consumeToken(b, MOUSE_RIGHT_PRESSED);
    if (!r) r = consumeToken(b, MOUSE_MIDDLE_PRESSED);
    if (!r) r = consumeToken(b, MOUSE_LEFT_RELEASE);
    if (!r) r = consumeToken(b, MOUSE_RIGHT_RELEASE);
    if (!r) r = consumeToken(b, MOUSE_MIDDLE_RELEASE);
    if (!r) r = consumeToken(b, NO_MOUSE);
    if (!r) r = consumeToken(b, MOUSE_ENTER);
    if (!r) r = consumeToken(b, MOUSE_LEAVE);
    if (!r) r = consumeToken(b, MOUSE_WHEEL_UP);
    if (!r) r = consumeToken(b, MOUSE_WHEEL_DOWN);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_LEFT_DOWN);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_RIGHT_DOWN);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_MIDDLE_DOWN);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_LEFT_RELEASE);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_RIGHT_RELEASE);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_MIDDLE_RELEASE);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_LEFT_PRESSED);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_RIGHT_PRESSED);
    if (!r) r = consumeToken(b, GLOBAL_MOUSE_MIDDLE_PRESSED);
    if (!r) r = consumeToken(b, KEY_LEFT_RELEASED);
    if (!r) r = consumeToken(b, KEY_LEFT_DOWN);
    if (!r) r = consumeToken(b, KEY_LEFT_PRESSED);
    if (!r) r = consumeToken(b, KEY_UP_RELEASED);
    if (!r) r = consumeToken(b, KEY_UP_DOWN);
    if (!r) r = consumeToken(b, KEY_UP_PRESSED);
    if (!r) r = consumeToken(b, KEY_RIGHT_RELEASED);
    if (!r) r = consumeToken(b, KEY_RIGHT_DOWN);
    if (!r) r = consumeToken(b, KEY_RIGHT_PRESSED);
    if (!r) r = consumeToken(b, KEY_DOWN_RELEASED);
    if (!r) r = consumeToken(b, KEY_DOWN_DOWN);
    if (!r) r = consumeToken(b, KEY_DOWN_PRESSED);
    if (!r) r = consumeToken(b, KEY_CONTROL_RELEASED);
    if (!r) r = consumeToken(b, KEY_CONTROL_DOWN);
    if (!r) r = consumeToken(b, KEY_CONTROL_PRESSED);
    if (!r) r = consumeToken(b, KEY_ALT_RELEASED);
    if (!r) r = consumeToken(b, KEY_ALT_DOWN);
    if (!r) r = consumeToken(b, KEY_ALT_PRESSED);
    if (!r) r = consumeToken(b, KEY_SHIFT_RELEASED);
    if (!r) r = consumeToken(b, KEY_SHIFT_DOWN);
    if (!r) r = consumeToken(b, KEY_SHIFT_PRESSED);
    if (!r) r = consumeToken(b, KEY_SPACE_RELEASED);
    if (!r) r = consumeToken(b, KEY_SPACE_DOWN);
    if (!r) r = consumeToken(b, KEY_SPACE_PRESSED);
    if (!r) r = consumeToken(b, KEY_ENTER_RELEASED);
    if (!r) r = consumeToken(b, KEY_ENTER_DOWN);
    if (!r) r = consumeToken(b, KEY_ENTER_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_0_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_0_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_0_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_1_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_1_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_1_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_2_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_2_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_2_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_3_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_3_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_3_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_4_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_4_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_4_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_5_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_5_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_5_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_6_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_6_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_6_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_7_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_7_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_7_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_8_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_8_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_8_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_9_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_9_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_9_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_SLASH_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_SLASH_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_SLASH_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MULT_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MULT_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MULT_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MINUS_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MINUS_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_MINUS_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_PLUS_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_PLUS_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_PLUS_PRESSED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_DOT_RELEASED);
    if (!r) r = consumeToken(b, KEY_KEYPAD_DOT_DOWN);
    if (!r) r = consumeToken(b, KEY_KEYPAD_DOT_PRESSED);
    if (!r) r = consumeToken(b, KEY_0_RELEASED);
    if (!r) r = consumeToken(b, KEY_0_DOWN);
    if (!r) r = consumeToken(b, KEY_0_PRESSED);
    if (!r) r = consumeToken(b, KEY_1_RELEASED);
    if (!r) r = consumeToken(b, KEY_1_DOWN);
    if (!r) r = consumeToken(b, KEY_1_PRESSED);
    if (!r) r = consumeToken(b, KEY_2_DOWN);
    if (!r) r = consumeToken(b, KEY_2_PRESSED);
    if (!r) r = consumeToken(b, KEY_3_UP);
    if (!r) r = consumeToken(b, KEY_3_DOWN);
    if (!r) r = consumeToken(b, KEY_3_PRESSED);
    if (!r) r = consumeToken(b, KEY_4_RELEASED);
    if (!r) r = consumeToken(b, KEY_4_DOWN);
    if (!r) r = consumeToken(b, KEY_4_PRESSED);
    if (!r) r = consumeToken(b, KEY_5_RELEASED);
    if (!r) r = consumeToken(b, KEY_5_DOWN);
    if (!r) r = consumeToken(b, KEY_5_PRESSED);
    if (!r) r = consumeToken(b, KEY_6_UP);
    if (!r) r = consumeToken(b, KEY_6_DOWN);
    if (!r) r = consumeToken(b, KEY_6_PRESSED);
    if (!r) r = consumeToken(b, KEY_7_RELEASED);
    if (!r) r = consumeToken(b, KEY_7_DOWN);
    if (!r) r = consumeToken(b, KEY_7_PRESSED);
    if (!r) r = consumeToken(b, KEY_8_RELEASED);
    if (!r) r = consumeToken(b, KEY_8_DOWN);
    if (!r) r = consumeToken(b, KEY_8_PRESSED);
    if (!r) r = consumeToken(b, KEY_9_UP);
    if (!r) r = consumeToken(b, KEY_9_DOWN);
    if (!r) r = consumeToken(b, KEY_9_PRESSED);
    if (!r) r = consumeToken(b, KEY_A_RELEASED);
    if (!r) r = consumeToken(b, KEY_A_DOWN);
    if (!r) r = consumeToken(b, KEY_A_PRESSED);
    if (!r) r = consumeToken(b, KEY_B_RELEASED);
    if (!r) r = consumeToken(b, KEY_B_DOWN);
    if (!r) r = consumeToken(b, KEY_B_PRESSED);
    if (!r) r = consumeToken(b, KEY_C_UP);
    if (!r) r = consumeToken(b, KEY_C_DOWN);
    if (!r) r = consumeToken(b, KEY_C_PRESSED);
    if (!r) r = consumeToken(b, KEY_D_RELEASED);
    if (!r) r = consumeToken(b, KEY_D_DOWN);
    if (!r) r = consumeToken(b, KEY_D_PRESSED);
    if (!r) r = consumeToken(b, KEY_E_RELEASED);
    if (!r) r = consumeToken(b, KEY_E_DOWN);
    if (!r) r = consumeToken(b, KEY_E_PRESSED);
    if (!r) r = consumeToken(b, KEY_F_UP);
    if (!r) r = consumeToken(b, KEY_F_DOWN);
    if (!r) r = consumeToken(b, KEY_F_PRESSED);
    if (!r) r = consumeToken(b, KEY_G_RELEASED);
    if (!r) r = consumeToken(b, KEY_G_DOWN);
    if (!r) r = consumeToken(b, KEY_G_PRESSED);
    if (!r) r = consumeToken(b, KEY_H_RELEASED);
    if (!r) r = consumeToken(b, KEY_H_DOWN);
    if (!r) r = consumeToken(b, KEY_H_PRESSED);
    if (!r) r = consumeToken(b, KEY_I_UP);
    if (!r) r = consumeToken(b, KEY_I_DOWN);
    if (!r) r = consumeToken(b, KEY_I_PRESSED);
    if (!r) r = consumeToken(b, KEY_J_RELEASED);
    if (!r) r = consumeToken(b, KEY_J_DOWN);
    if (!r) r = consumeToken(b, KEY_J_PRESSED);
    if (!r) r = consumeToken(b, KEY_K_RELEASED);
    if (!r) r = consumeToken(b, KEY_K_DOWN);
    if (!r) r = consumeToken(b, KEY_K_PRESSED);
    if (!r) r = consumeToken(b, KEY_L_UP);
    if (!r) r = consumeToken(b, KEY_L_DOWN);
    if (!r) r = consumeToken(b, KEY_L_PRESSED);
    if (!r) r = consumeToken(b, KEY_M_RELEASED);
    if (!r) r = consumeToken(b, KEY_M_DOWN);
    if (!r) r = consumeToken(b, KEY_M_PRESSED);
    if (!r) r = consumeToken(b, KEY_N_RELEASED);
    if (!r) r = consumeToken(b, KEY_N_DOWN);
    if (!r) r = consumeToken(b, KEY_N_PRESSED);
    if (!r) r = consumeToken(b, KEY_O_UP);
    if (!r) r = consumeToken(b, KEY_O_DOWN);
    if (!r) r = consumeToken(b, KEY_O_PRESSED);
    if (!r) r = consumeToken(b, KEY_P_RELEASED);
    if (!r) r = consumeToken(b, KEY_P_DOWN);
    if (!r) r = consumeToken(b, KEY_P_PRESSED);
    if (!r) r = consumeToken(b, KEY_Q_RELEASED);
    if (!r) r = consumeToken(b, KEY_Q_DOWN);
    if (!r) r = consumeToken(b, KEY_Q_PRESSED);
    if (!r) r = consumeToken(b, KEY_R_UP);
    if (!r) r = consumeToken(b, KEY_R_DOWN);
    if (!r) r = consumeToken(b, KEY_R_PRESSED);
    if (!r) r = consumeToken(b, KEY_S_RELEASED);
    if (!r) r = consumeToken(b, KEY_S_DOWN);
    if (!r) r = consumeToken(b, KEY_S_PRESSED);
    if (!r) r = consumeToken(b, KEY_T_RELEASED);
    if (!r) r = consumeToken(b, KEY_T_DOWN);
    if (!r) r = consumeToken(b, KEY_T_PRESSED);
    if (!r) r = consumeToken(b, KEY_U_UP);
    if (!r) r = consumeToken(b, KEY_U_DOWN);
    if (!r) r = consumeToken(b, KEY_U_PRESSED);
    if (!r) r = consumeToken(b, KEY_V_RELEASED);
    if (!r) r = consumeToken(b, KEY_V_DOWN);
    if (!r) r = consumeToken(b, KEY_V_PRESSED);
    if (!r) r = consumeToken(b, KEY_W_RELEASED);
    if (!r) r = consumeToken(b, KEY_W_DOWN);
    if (!r) r = consumeToken(b, KEY_W_PRESSED);
    if (!r) r = consumeToken(b, KEY_X_UP);
    if (!r) r = consumeToken(b, KEY_X_DOWN);
    if (!r) r = consumeToken(b, KEY_X_PRESSED);
    if (!r) r = consumeToken(b, KEY_Y_RELEASED);
    if (!r) r = consumeToken(b, KEY_Y_DOWN);
    if (!r) r = consumeToken(b, KEY_Y_PRESSED);
    if (!r) r = consumeToken(b, KEY_Z_RELEASED);
    if (!r) r = consumeToken(b, KEY_Z_DOWN);
    if (!r) r = consumeToken(b, KEY_Z_PRESSED);
    if (!r) r = consumeToken(b, KEY_F1_UP);
    if (!r) r = consumeToken(b, KEY_F1_DOWN);
    if (!r) r = consumeToken(b, KEY_F1_PRESSED);
    if (!r) r = consumeToken(b, KEY_F2_RELEASED);
    if (!r) r = consumeToken(b, KEY_F2_DOWN);
    if (!r) r = consumeToken(b, KEY_F2_PRESSED);
    if (!r) r = consumeToken(b, KEY_F3_RELEASED);
    if (!r) r = consumeToken(b, KEY_F3_DOWN);
    if (!r) r = consumeToken(b, KEY_F3_PRESSED);
    if (!r) r = consumeToken(b, KEY_F4_RELEASED);
    if (!r) r = consumeToken(b, KEY_F4_DOWN);
    if (!r) r = consumeToken(b, KEY_F4_PRESSED);
    if (!r) r = consumeToken(b, KEY_F5_RELEASED);
    if (!r) r = consumeToken(b, KEY_F5_DOWN);
    if (!r) r = consumeToken(b, KEY_F5_PRESSED);
    if (!r) r = consumeToken(b, KEY_F6_RELEASED);
    if (!r) r = consumeToken(b, KEY_F6_DOWN);
    if (!r) r = consumeToken(b, KEY_F6_PRESSED);
    if (!r) r = consumeToken(b, KEY_F7_RELEASED);
    if (!r) r = consumeToken(b, KEY_F7_DOWN);
    if (!r) r = consumeToken(b, KEY_F7_PRESSED);
    if (!r) r = consumeToken(b, KEY_F8_RELEASED);
    if (!r) r = consumeToken(b, KEY_F8_DOWN);
    if (!r) r = consumeToken(b, KEY_F8_PRESSED);
    if (!r) r = consumeToken(b, KEY_F9_RELEASED);
    if (!r) r = consumeToken(b, KEY_F9_DOWN);
    if (!r) r = consumeToken(b, KEY_F9_PRESSED);
    if (!r) r = consumeToken(b, KEY_F10_RELEASED);
    if (!r) r = consumeToken(b, KEY_F10_DOWN);
    if (!r) r = consumeToken(b, KEY_F10_PRESSED);
    if (!r) r = consumeToken(b, KEY_F11_RELEASED);
    if (!r) r = consumeToken(b, KEY_F11_DOWN);
    if (!r) r = consumeToken(b, KEY_F11_PRESSED);
    if (!r) r = consumeToken(b, KEY_F12_RELEASED);
    if (!r) r = consumeToken(b, KEY_F12_DOWN);
    if (!r) r = consumeToken(b, KEY_F12_PRESSED);
    if (!r) r = consumeToken(b, KEY_BACKSPACE_RELEASED);
    if (!r) r = consumeToken(b, KEY_BACKSPACE_DOWN);
    if (!r) r = consumeToken(b, KEY_BACKSPACE_PRESSED);
    if (!r) r = consumeToken(b, KEY_ESCAPE_RELEASED);
    if (!r) r = consumeToken(b, KEY_ESCAPE_DOWN);
    if (!r) r = consumeToken(b, KEY_ESCAPE_PRESSED);
    if (!r) r = consumeToken(b, KEY_HOME_RELEASED);
    if (!r) r = consumeToken(b, KEY_HOME_DOWN);
    if (!r) r = consumeToken(b, KEY_HOME_PRESSED);
    if (!r) r = consumeToken(b, KEY_END_RELEASED);
    if (!r) r = consumeToken(b, KEY_END_DOWN);
    if (!r) r = consumeToken(b, KEY_END_PRESSED);
    if (!r) r = consumeToken(b, KEY_PAGE_UP_RELEASED);
    if (!r) r = consumeToken(b, KEY_PAGE_UP_DOWN);
    if (!r) r = consumeToken(b, KEY_PAGE_UP_PRESSED);
    if (!r) r = consumeToken(b, KEY_PAGE_DOWN_RELEASED);
    if (!r) r = consumeToken(b, KEY_PAGE_DOWN_DOWN);
    if (!r) r = consumeToken(b, KEY_PAGE_DOWN_PRESSED);
    if (!r) r = consumeToken(b, KEY_DELETE_RELEASED);
    if (!r) r = consumeToken(b, KEY_DELETE_DOWN);
    if (!r) r = consumeToken(b, KEY_DELETE_PRESSED);
    if (!r) r = consumeToken(b, KEY_INSERT_RELEASED);
    if (!r) r = consumeToken(b, KEY_INSERT_DOWN);
    if (!r) r = consumeToken(b, KEY_INSERT_PRESSED);
    if (!r) r = consumeToken(b, KEY_NO_RELEASED);
    if (!r) r = consumeToken(b, KEY_NO_DOWN);
    if (!r) r = consumeToken(b, KEY_NO_PRESSED);
    if (!r) r = consumeToken(b, KEY_ANY_RELEASED);
    if (!r) r = consumeToken(b, KEY_ANY_DOWN);
    if (!r) r = consumeToken(b, KEY_ANY_PRESSED);
    if (!r) r = consumeToken(b, GESTURE_TAP);
    if (!r) r = consumeToken(b, GESTURE_DOUBLE_TAP);
    if (!r) r = consumeToken(b, GESTURE_DRAG_START);
    if (!r) r = consumeToken(b, GESTURE_DRAGGING);
    if (!r) r = consumeToken(b, GESTURE_DRAG_END);
    if (!r) r = consumeToken(b, GESTURE_FLICK);
    if (!r) r = consumeToken(b, GESTURE_PINCH_START);
    if (!r) r = consumeToken(b, GESTURE_PINCH_IN);
    if (!r) r = consumeToken(b, GESTURE_PINCH_OUT);
    if (!r) r = consumeToken(b, GESTURE_PINCH_END);
    if (!r) r = consumeToken(b, GESTURE_ROTATE_START);
    if (!r) r = consumeToken(b, GESTURE_ROTATING);
    if (!r) r = consumeToken(b, GESTURE_ROTATE_END);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_TAP);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_DOUBLE_TAP);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_DRAG_START);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_DRAGGING);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_DRAG_END);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_FLICK);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_PINCH_START);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_PINCH_IN);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_PINCH_OUT);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_PINCH_END);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_ROTATE_START);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_ROTATING);
    if (!r) r = consumeToken(b, GLOBAL_GESTURE_ROTATE_END);
    if (!r) r = consumeToken(b, OUTSIDE_ROOM);
    if (!r) r = consumeToken(b, INTERSECT_BOUNDARY);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_0);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_1);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_2);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_3);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_4);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_5);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_6);
    if (!r) r = consumeToken(b, OUTSIDE_VIEW_7);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_0_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_1_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_2_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_3_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_4_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_5_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_6_BOUNDARY);
    if (!r) r = consumeToken(b, INTERSECT_VIEW_7_BOUNDARY);
    if (!r) r = consumeToken(b, GAME_START);
    if (!r) r = consumeToken(b, GAME_END);
    if (!r) r = consumeToken(b, ROOM_START);
    if (!r) r = consumeToken(b, ROOM_END);
    if (!r) r = consumeToken(b, ANIMATION_END);
    if (!r) r = consumeToken(b, ANIMATION_UPDATE);
    if (!r) r = consumeToken(b, ANIMATION_EVENT);
    if (!r) r = consumeToken(b, PATH_ENDED);
    if (!r) r = consumeToken(b, USER_EVENT_0);
    if (!r) r = consumeToken(b, USER_EVENT_1);
    if (!r) r = consumeToken(b, USER_EVENT_2);
    if (!r) r = consumeToken(b, USER_EVENT_3);
    if (!r) r = consumeToken(b, USER_EVENT_4);
    if (!r) r = consumeToken(b, USER_EVENT_5);
    if (!r) r = consumeToken(b, USER_EVENT_6);
    if (!r) r = consumeToken(b, USER_EVENT_7);
    if (!r) r = consumeToken(b, USER_EVENT_8);
    if (!r) r = consumeToken(b, USER_EVENT_9);
    if (!r) r = consumeToken(b, USER_EVENT_10);
    if (!r) r = consumeToken(b, USER_EVENT_11);
    if (!r) r = consumeToken(b, USER_EVENT_12);
    if (!r) r = consumeToken(b, USER_EVENT_13);
    if (!r) r = consumeToken(b, USER_EVENT_14);
    if (!r) r = consumeToken(b, USER_EVENT_15);
    if (!r) r = consumeToken(b, ASYNC_AUDIO_PLAYBACK);
    if (!r) r = consumeToken(b, ASYNC_AUDIO_RECORDING);
    if (!r) r = consumeToken(b, ASYNC_CLOUD);
    if (!r) r = consumeToken(b, ASYNC_DIALOG);
    if (!r) r = consumeToken(b, ASYNC_HTTP);
    if (!r) r = consumeToken(b, ASYNC_IN_APP_PURCHASE);
    if (!r) r = consumeToken(b, ASYNC_IMAGE_LOADED);
    if (!r) r = consumeToken(b, ASYNC_NETWORKING);
    if (!r) r = consumeToken(b, ASYNC_PUSH_NOTIFICATION);
    if (!r) r = consumeToken(b, ASYNC_SAVE_LOAD);
    if (!r) r = consumeToken(b, ASYNC_SOCIAL);
    if (!r) r = consumeToken(b, ASYNC_STEAM);
    if (!r) r = consumeToken(b, ASYNC_SYSTEM);
    if (!r) r = collision_event(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // comp | and | andand | or | oror | plus | minus | mult | div | mod | xor | lshift | rshift
  public static boolean exprOp(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "exprOp")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EXPR_OP, "<expr op>");
    r = consumeToken(b, COMP);
    if (!r) r = consumeToken(b, AND);
    if (!r) r = consumeToken(b, ANDAND);
    if (!r) r = consumeToken(b, OR);
    if (!r) r = consumeToken(b, OROR);
    if (!r) r = consumeToken(b, PLUS);
    if (!r) r = consumeToken(b, MINUS);
    if (!r) r = consumeToken(b, MULT);
    if (!r) r = consumeToken(b, DIV);
    if (!r) r = consumeToken(b, MOD);
    if (!r) r = consumeToken(b, XOR);
    if (!r) r = consumeToken(b, LSHIFT);
    if (!r) r = consumeToken(b, RSHIFT);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // for lrbr conlessStatement? semicon expression? semicon conlessStatement? rrbr statement
  public static boolean forStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement")) return false;
    if (!nextTokenIs(b, FOR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, FOR, LRBR);
    r = r && forStatement_2(b, l + 1);
    r = r && consumeToken(b, SEMICON);
    r = r && forStatement_4(b, l + 1);
    r = r && consumeToken(b, SEMICON);
    r = r && forStatement_6(b, l + 1);
    r = r && consumeToken(b, RRBR);
    r = r && statement(b, l + 1);
    exit_section_(b, m, FOR_STATEMENT, r);
    return r;
  }

  // conlessStatement?
  private static boolean forStatement_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_2")) return false;
    conlessStatement(b, l + 1);
    return true;
  }

  // expression?
  private static boolean forStatement_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_4")) return false;
    expression(b, l + 1, -1);
    return true;
  }

  // conlessStatement?
  private static boolean forStatement_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_6")) return false;
    conlessStatement(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // identifier_plus
  public static boolean funcArgDecl(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "funcArgDecl")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNC_ARG_DECL, "<func arg decl>");
    r = identifier_plus(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // identifier (comma identifier)*
  public static boolean funcReturnName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "funcReturnName")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    r = r && funcReturnName_1(b, l + 1);
    exit_section_(b, m, FUNC_RETURN_NAME, r);
    return r;
  }

  // (comma identifier)*
  private static boolean funcReturnName_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "funcReturnName_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!funcReturnName_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "funcReturnName_1", c)) break;
    }
    return true;
  }

  // comma identifier
  private static boolean funcReturnName_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "funcReturnName_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COMMA, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // colon funcReturnName
  public static boolean funcReturnval(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "funcReturnval")) return false;
    if (!nextTokenIs(b, COLON)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON);
    r = r && funcReturnName(b, l + 1);
    exit_section_(b, m, FUNC_RETURNVAL, r);
    return r;
  }

  /* ********************************************************** */
  // expression (comma expression)*
  public static boolean functionArgList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_ARG_LIST, "<function arg list>");
    r = expression(b, l + 1, -1);
    r = r && functionArgList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (comma expression)*
  private static boolean functionArgList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgList_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!functionArgList_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "functionArgList_1", c)) break;
    }
    return true;
  }

  // comma expression
  private static boolean functionArgList_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgList_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (funcArgDecl (comma funcArgDecl)*)?
  public static boolean functionArgListDecl(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgListDecl")) return false;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_ARG_LIST_DECL, "<function arg list decl>");
    functionArgListDecl_0(b, l + 1);
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // funcArgDecl (comma funcArgDecl)*
  private static boolean functionArgListDecl_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgListDecl_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = funcArgDecl(b, l + 1);
    r = r && functionArgListDecl_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (comma funcArgDecl)*
  private static boolean functionArgListDecl_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgListDecl_0_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!functionArgListDecl_0_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "functionArgListDecl_0_1", c)) break;
    }
    return true;
  }

  // comma funcArgDecl
  private static boolean functionArgListDecl_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionArgListDecl_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA);
    r = r && funcArgDecl(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // functionDeclarationName lrbr functionArgListDecl rrbr funcReturnval
  public static boolean functionDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_DECLARATION, "<function declaration>");
    r = functionDeclarationName(b, l + 1);
    r = r && consumeToken(b, LRBR);
    r = r && functionArgListDecl(b, l + 1);
    r = r && consumeToken(b, RRBR);
    r = r && funcReturnval(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // identifier | game_end | mouse_wheel_down | mouse_wheel_up
  public static boolean functionDeclarationName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclarationName")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_DECLARATION_NAME, "<function declaration name>");
    r = consumeToken(b, IDENTIFIER);
    if (!r) r = consumeToken(b, GAME_END);
    if (!r) r = consumeToken(b, MOUSE_WHEEL_DOWN);
    if (!r) r = consumeToken(b, MOUSE_WHEEL_UP);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // globalvar identifier semicon?
  public static boolean globalvarDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "globalvarDeclaration")) return false;
    if (!nextTokenIs(b, GLOBALVAR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, GLOBALVAR, IDENTIFIER);
    r = r && globalvarDeclaration_2(b, l + 1);
    exit_section_(b, m, GLOBALVAR_DECLARATION, r);
    return r;
  }

  // semicon?
  private static boolean globalvarDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "globalvarDeclaration_2")) return false;
    consumeToken(b, SEMICON);
    return true;
  }

  /* ********************************************************** */
  // gmlResources | gmlFunctions | gmlInstanceVariables | gmlBuiltinConstants | gmlObject | gmlScript
  static boolean gmlBase(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlBase")) return false;
    boolean r;
    r = gmlResources(b, l + 1);
    if (!r) r = gmlFunctions(b, l + 1);
    if (!r) r = gmlInstanceVariables(b, l + 1);
    if (!r) r = gmlBuiltinConstants(b, l + 1);
    if (!r) r = gmlObject(b, l + 1);
    if (!r) r = gmlScript(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // identifier
  public static boolean gmlBuiltinConstant(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlBuiltinConstant")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, GML_BUILTIN_CONSTANT, r);
    return r;
  }

  /* ********************************************************** */
  // builtin_constants gmlBuiltinConstant*
  public static boolean gmlBuiltinConstants(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlBuiltinConstants")) return false;
    if (!nextTokenIs(b, BUILTIN_CONSTANTS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, BUILTIN_CONSTANTS);
    r = r && gmlBuiltinConstants_1(b, l + 1);
    exit_section_(b, m, GML_BUILTIN_CONSTANTS, r);
    return r;
  }

  // gmlBuiltinConstant*
  private static boolean gmlBuiltinConstants_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlBuiltinConstants_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!gmlBuiltinConstant(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlBuiltinConstants_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // functions functionDeclaration*
  public static boolean gmlFunctions(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlFunctions")) return false;
    if (!nextTokenIs(b, FUNCTIONS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, FUNCTIONS);
    r = r && gmlFunctions_1(b, l + 1);
    exit_section_(b, m, GML_FUNCTIONS, r);
    return r;
  }

  // functionDeclaration*
  private static boolean gmlFunctions_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlFunctions_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!functionDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlFunctions_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // identifier
  public static boolean gmlInstanceVariable(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlInstanceVariable")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, GML_INSTANCE_VARIABLE, r);
    return r;
  }

  /* ********************************************************** */
  // instance_variables gmlInstanceVariable*
  public static boolean gmlInstanceVariables(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlInstanceVariables")) return false;
    if (!nextTokenIs(b, INSTANCE_VARIABLES)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, INSTANCE_VARIABLES);
    r = r && gmlInstanceVariables_1(b, l + 1);
    exit_section_(b, m, GML_INSTANCE_VARIABLES, r);
    return r;
  }

  // gmlInstanceVariable*
  private static boolean gmlInstanceVariables_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlInstanceVariables_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!gmlInstanceVariable(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlInstanceVariables_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // eventDeclaration+
  public static boolean gmlObject(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlObject")) return false;
    if (!nextTokenIs(b, EVENT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = eventDeclaration(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!eventDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlObject", c)) break;
    }
    exit_section_(b, m, GML_OBJECT, r);
    return r;
  }

  /* ********************************************************** */
  // identifier
  public static boolean gmlResource(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlResource")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, GML_RESOURCE, r);
    return r;
  }

  /* ********************************************************** */
  // resources gmlResource*
  public static boolean gmlResources(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlResources")) return false;
    if (!nextTokenIs(b, RESOURCES)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, RESOURCES);
    r = r && gmlResources_1(b, l + 1);
    exit_section_(b, m, GML_RESOURCES, r);
    return r;
  }

  // gmlResource*
  private static boolean gmlResources_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlResources_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!gmlResource(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlResources_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // (statement | scriptFunctionDeclaration)*
  public static boolean gmlScript(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlScript")) return false;
    Marker m = enter_section_(b, l, _NONE_, GML_SCRIPT, "<gml script>");
    while (true) {
      int c = current_position_(b);
      if (!gmlScript_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "gmlScript", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // statement | scriptFunctionDeclaration
  private static boolean gmlScript_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "gmlScript_0")) return false;
    boolean r;
    r = statement(b, l + 1);
    if (!r) r = scriptFunctionDeclaration(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // (other | identifier) (lsbr expression rsbr)?
  public static boolean identifierRef(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierRef")) return false;
    if (!nextTokenIs(b, "<identifier ref>", IDENTIFIER, OTHER)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, IDENTIFIER_REF, "<identifier ref>");
    r = identifierRef_0(b, l + 1);
    r = r && identifierRef_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // other | identifier
  private static boolean identifierRef_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierRef_0")) return false;
    boolean r;
    r = consumeToken(b, OTHER);
    if (!r) r = consumeToken(b, IDENTIFIER);
    return r;
  }

  // (lsbr expression rsbr)?
  private static boolean identifierRef_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierRef_1")) return false;
    identifierRef_1_0(b, l + 1);
    return true;
  }

  // lsbr expression rsbr
  private static boolean identifierRef_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierRef_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LSBR);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RSBR);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (identifier | dot | eventType | exprOp | assignOp | minus | plus | number | lsbr | rsbr | default | script )*
  public static boolean identifier_plus(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifier_plus")) return false;
    Marker m = enter_section_(b, l, _NONE_, IDENTIFIER_PLUS, "<identifier plus>");
    while (true) {
      int c = current_position_(b);
      if (!identifier_plus_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "identifier_plus", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // identifier | dot | eventType | exprOp | assignOp | minus | plus | number | lsbr | rsbr | default | script
  private static boolean identifier_plus_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifier_plus_0")) return false;
    boolean r;
    r = consumeToken(b, IDENTIFIER);
    if (!r) r = consumeToken(b, DOT);
    if (!r) r = eventType(b, l + 1);
    if (!r) r = exprOp(b, l + 1);
    if (!r) r = assignOp(b, l + 1);
    if (!r) r = consumeToken(b, MINUS);
    if (!r) r = consumeToken(b, PLUS);
    if (!r) r = consumeToken(b, NUMBER);
    if (!r) r = consumeToken(b, LSBR);
    if (!r) r = consumeToken(b, RSBR);
    if (!r) r = consumeToken(b, DEFAULT);
    if (!r) r = consumeToken(b, SCRIPT);
    return r;
  }

  /* ********************************************************** */
  // if expression statement (else statement)?
  public static boolean ifStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement")) return false;
    if (!nextTokenIs(b, IF)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IF);
    r = r && expression(b, l + 1, -1);
    r = r && statement(b, l + 1);
    r = r && ifStatement_3(b, l + 1);
    exit_section_(b, m, IF_STATEMENT, r);
    return r;
  }

  // (else statement)?
  private static boolean ifStatement_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_3")) return false;
    ifStatement_3_0(b, l + 1);
    return true;
  }

  // else statement
  private static boolean ifStatement_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // function lrbr functionArgList? rrbr statementBlock
  public static boolean inlineFunctionDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inlineFunctionDeclaration")) return false;
    if (!nextTokenIs(b, FUNCTION)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, FUNCTION, LRBR);
    r = r && inlineFunctionDeclaration_2(b, l + 1);
    r = r && consumeToken(b, RRBR);
    r = r && statementBlock(b, l + 1);
    exit_section_(b, m, INLINE_FUNCTION_DECLARATION, r);
    return r;
  }

  // functionArgList?
  private static boolean inlineFunctionDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inlineFunctionDeclaration_2")) return false;
    functionArgList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // plusplus | minusminus
  public static boolean plusMinusOp(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "plusMinusOp")) return false;
    if (!nextTokenIs(b, "<plus minus op>", MINUSMINUS, PLUSPLUS)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PLUS_MINUS_OP, "<plus minus op>");
    r = consumeToken(b, PLUSPLUS);
    if (!r) r = consumeToken(b, MINUSMINUS);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // repeat lrbr expression rrbr statement
  public static boolean repeatStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "repeatStatement")) return false;
    if (!nextTokenIs(b, REPEAT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, REPEAT, LRBR);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RRBR);
    r = r && statement(b, l + 1);
    exit_section_(b, m, REPEAT_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // return expression semicon?
  public static boolean returnStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnStatement")) return false;
    if (!nextTokenIs(b, RETURN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, RETURN);
    r = r && expression(b, l + 1, -1);
    r = r && returnStatement_2(b, l + 1);
    exit_section_(b, m, RETURN_STATEMENT, r);
    return r;
  }

  // semicon?
  private static boolean returnStatement_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnStatement_2")) return false;
    consumeToken(b, SEMICON);
    return true;
  }

  /* ********************************************************** */
  // scriptRef lrbr functionArgList? rrbr
  public static boolean scriptCall(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "scriptCall")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = scriptRef(b, l + 1);
    r = r && consumeToken(b, LRBR);
    r = r && scriptCall_2(b, l + 1);
    r = r && consumeToken(b, RRBR);
    exit_section_(b, m, SCRIPT_CALL, r);
    return r;
  }

  // functionArgList?
  private static boolean scriptCall_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "scriptCall_2")) return false;
    functionArgList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // function identifier lrbr functionArgList? rrbr statementBlock
  public static boolean scriptFunctionDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "scriptFunctionDeclaration")) return false;
    if (!nextTokenIs(b, FUNCTION)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, FUNCTION, IDENTIFIER, LRBR);
    r = r && scriptFunctionDeclaration_3(b, l + 1);
    r = r && consumeToken(b, RRBR);
    r = r && statementBlock(b, l + 1);
    exit_section_(b, m, SCRIPT_FUNCTION_DECLARATION, r);
    return r;
  }

  // functionArgList?
  private static boolean scriptFunctionDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "scriptFunctionDeclaration_3")) return false;
    functionArgList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // identifier
  public static boolean scriptRef(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "scriptRef")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, SCRIPT_REF, r);
    return r;
  }

  /* ********************************************************** */
  // conlessStatement semicon?
  public static boolean statement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, STATEMENT, "<statement>");
    r = conlessStatement(b, l + 1);
    r = r && statement_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // semicon?
  private static boolean statement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_1")) return false;
    consumeToken(b, SEMICON);
    return true;
  }

  /* ********************************************************** */
  // lcbr statement* rcbr
  public static boolean statementBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statementBlock")) return false;
    if (!nextTokenIs(b, LCBR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LCBR);
    r = r && statementBlock_1(b, l + 1);
    r = r && consumeToken(b, RCBR);
    exit_section_(b, m, STATEMENT_BLOCK, r);
    return r;
  }

  // statement*
  private static boolean statementBlock_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statementBlock_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "statementBlock_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // identifier colon expression
  public static boolean structFieldDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "structFieldDeclaration")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, IDENTIFIER, COLON);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, STRUCT_FIELD_DECLARATION, r);
    return r;
  }

  /* ********************************************************** */
  // switch lrbr expression rrbr lcbr caseStatement* defaultStatement? rcbr
  public static boolean switchStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement")) return false;
    if (!nextTokenIs(b, SWITCH)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, SWITCH, LRBR);
    r = r && expression(b, l + 1, -1);
    r = r && consumeTokens(b, 0, RRBR, LCBR);
    r = r && switchStatement_5(b, l + 1);
    r = r && switchStatement_6(b, l + 1);
    r = r && consumeToken(b, RCBR);
    exit_section_(b, m, SWITCH_STATEMENT, r);
    return r;
  }

  // caseStatement*
  private static boolean switchStatement_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement_5")) return false;
    while (true) {
      int c = current_position_(b);
      if (!caseStatement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "switchStatement_5", c)) break;
    }
    return true;
  }

  // defaultStatement?
  private static boolean switchStatement_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement_6")) return false;
    defaultStatement(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // eqeq | lteq | gteq | noteq | lt | gt
  public static boolean testOp(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "testOp")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TEST_OP, "<test op>");
    r = consumeToken(b, EQEQ);
    if (!r) r = consumeToken(b, LTEQ);
    if (!r) r = consumeToken(b, GTEQ);
    if (!r) r = consumeToken(b, NOTEQ);
    if (!r) r = consumeToken(b, LT);
    if (!r) r = consumeToken(b, GT);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // (var)? identifier afterIndentifier? assignOp (inlineFunctionDeclaration | expression)
  public static boolean varDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "varDeclaration")) return false;
    if (!nextTokenIs(b, "<var declaration>", IDENTIFIER, VAR)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VAR_DECLARATION, "<var declaration>");
    r = varDeclaration_0(b, l + 1);
    r = r && consumeToken(b, IDENTIFIER);
    r = r && varDeclaration_2(b, l + 1);
    r = r && assignOp(b, l + 1);
    r = r && varDeclaration_4(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (var)?
  private static boolean varDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "varDeclaration_0")) return false;
    consumeToken(b, VAR);
    return true;
  }

  // afterIndentifier?
  private static boolean varDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "varDeclaration_2")) return false;
    afterIndentifier(b, l + 1);
    return true;
  }

  // inlineFunctionDeclaration | expression
  private static boolean varDeclaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "varDeclaration_4")) return false;
    boolean r;
    r = inlineFunctionDeclaration(b, l + 1);
    if (!r) r = expression(b, l + 1, -1);
    return r;
  }

  /* ********************************************************** */
  // while lrbr expression rrbr statement
  public static boolean whileStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement")) return false;
    if (!nextTokenIs(b, WHILE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, WHILE, LRBR);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RRBR);
    r = r && statement(b, l + 1);
    exit_section_(b, m, WHILE_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // with expression statement
  public static boolean withStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "withStatement")) return false;
    if (!nextTokenIs(b, WITH)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, WITH);
    r = r && expression(b, l + 1, -1);
    r = r && statement(b, l + 1);
    exit_section_(b, m, WITH_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11
  public static boolean zero_eleven(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "zero_eleven")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ZERO_ELEVEN, "<zero eleven>");
    r = consumeToken(b, "0");
    if (!r) r = consumeToken(b, "1");
    if (!r) r = consumeToken(b, "2");
    if (!r) r = consumeToken(b, "3");
    if (!r) r = consumeToken(b, "4");
    if (!r) r = consumeToken(b, "5");
    if (!r) r = consumeToken(b, "6");
    if (!r) r = consumeToken(b, "7");
    if (!r) r = consumeToken(b, "8");
    if (!r) r = consumeToken(b, "9");
    if (!r) r = consumeToken(b, "10");
    if (!r) r = consumeToken(b, "11");
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // Expression root: expression
  // Operator priority table:
  // 0: POSTFIX(assignExpression)
  // 1: BINARY(conditionalExpression)
  // 2: ATOM(roundBracketExpression)
  // 3: BINARY(computeExpression)
  // 4: ATOM(structDeclaration)
  // 5: BINARY(testExpression)
  // 6: PREFIX(plusMinusExpression)
  // 7: POSTFIX(plusMinusRevExpression)
  // 8: ATOM(literalExpression)
  // 9: ATOM(referenceExpression)
  public static boolean expression(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression")) return false;
    addVariant(b, "<expression>");
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, "<expression>");
    r = roundBracketExpression(b, l + 1);
    if (!r) r = structDeclaration(b, l + 1);
    if (!r) r = plusMinusExpression(b, l + 1);
    if (!r) r = literalExpression(b, l + 1);
    if (!r) r = referenceExpression(b, l + 1);
    p = r;
    r = r && expression_0(b, l + 1, g);
    exit_section_(b, l, m, null, r, p, null);
    return r || p;
  }

  public static boolean expression_0(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression_0")) return false;
    boolean r = true;
    while (true) {
      Marker m = enter_section_(b, l, _LEFT_, null);
      if (g < 0 && assignExpression_0(b, l + 1)) {
        r = true;
        exit_section_(b, l, m, ASSIGN_EXPRESSION, r, true, null);
      }
      else if (g < 1 && consumeTokenSmart(b, QUESTION)) {
        r = report_error_(b, expression(b, l, 1));
        r = conditionalExpression_1(b, l + 1) && r;
        exit_section_(b, l, m, CONDITIONAL_EXPRESSION, r, true, null);
      }
      else if (g < 3 && exprOp(b, l + 1)) {
        r = expression(b, l, 3);
        exit_section_(b, l, m, COMPUTE_EXPRESSION, r, true, null);
      }
      else if (g < 5 && testOp(b, l + 1)) {
        r = expression(b, l, 5);
        exit_section_(b, l, m, TEST_EXPRESSION, r, true, null);
      }
      else if (g < 7 && plusMinusOp(b, l + 1)) {
        r = true;
        exit_section_(b, l, m, PLUS_MINUS_REV_EXPRESSION, r, true, null);
      }
      else {
        exit_section_(b, l, m, null, false, false, null);
        break;
      }
    }
    return r;
  }

  // assignOp (inlineFunctionDeclaration | expression)
  private static boolean assignExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assignOp(b, l + 1);
    r = r && assignExpression_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // inlineFunctionDeclaration | expression
  private static boolean assignExpression_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignExpression_0_1")) return false;
    boolean r;
    r = inlineFunctionDeclaration(b, l + 1);
    if (!r) r = expression(b, l + 1, -1);
    return r;
  }

  // colon expression
  private static boolean conditionalExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "conditionalExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  // lrbr expression* rrbr
  public static boolean roundBracketExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "roundBracketExpression")) return false;
    if (!nextTokenIsSmart(b, LRBR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, LRBR);
    r = r && roundBracketExpression_1(b, l + 1);
    r = r && consumeToken(b, RRBR);
    exit_section_(b, m, ROUND_BRACKET_EXPRESSION, r);
    return r;
  }

  // expression*
  private static boolean roundBracketExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "roundBracketExpression_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!expression(b, l + 1, -1)) break;
      if (!empty_element_parsed_guard_(b, "roundBracketExpression_1", c)) break;
    }
    return true;
  }

  // lcbr (structFieldDeclaration comma)* structFieldDeclaration? rcbr
  public static boolean structDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "structDeclaration")) return false;
    if (!nextTokenIsSmart(b, LCBR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, LCBR);
    r = r && structDeclaration_1(b, l + 1);
    r = r && structDeclaration_2(b, l + 1);
    r = r && consumeToken(b, RCBR);
    exit_section_(b, m, STRUCT_DECLARATION, r);
    return r;
  }

  // (structFieldDeclaration comma)*
  private static boolean structDeclaration_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "structDeclaration_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!structDeclaration_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "structDeclaration_1", c)) break;
    }
    return true;
  }

  // structFieldDeclaration comma
  private static boolean structDeclaration_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "structDeclaration_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = structFieldDeclaration(b, l + 1);
    r = r && consumeToken(b, COMMA);
    exit_section_(b, m, null, r);
    return r;
  }

  // structFieldDeclaration?
  private static boolean structDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "structDeclaration_2")) return false;
    structFieldDeclaration(b, l + 1);
    return true;
  }

  public static boolean plusMinusExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "plusMinusExpression")) return false;
    if (!nextTokenIsSmart(b, MINUSMINUS, PLUSPLUS)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = plusMinusOp(b, l + 1);
    p = r;
    r = p && expression(b, l, 6);
    exit_section_(b, l, m, PLUS_MINUS_EXPRESSION, r, p, null);
    return r || p;
  }

  // not? defCosnt|number|string
  public static boolean literalExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LITERAL_EXPRESSION, "<literal expression>");
    r = literalExpression_0(b, l + 1);
    if (!r) r = consumeTokenSmart(b, NUMBER);
    if (!r) r = consumeTokenSmart(b, STRING);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // not? defCosnt
  private static boolean literalExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = literalExpression_0_0(b, l + 1);
    r = r && defCosnt(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // not?
  private static boolean literalExpression_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalExpression_0_0")) return false;
    consumeTokenSmart(b, NOT);
    return true;
  }

  // not? (scriptCall | (identifierRef afterIndentifier?))
  public static boolean referenceExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "referenceExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, REFERENCE_EXPRESSION, "<reference expression>");
    r = referenceExpression_0(b, l + 1);
    r = r && referenceExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // not?
  private static boolean referenceExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "referenceExpression_0")) return false;
    consumeTokenSmart(b, NOT);
    return true;
  }

  // scriptCall | (identifierRef afterIndentifier?)
  private static boolean referenceExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "referenceExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = scriptCall(b, l + 1);
    if (!r) r = referenceExpression_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifierRef afterIndentifier?
  private static boolean referenceExpression_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "referenceExpression_1_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifierRef(b, l + 1);
    r = r && referenceExpression_1_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // afterIndentifier?
  private static boolean referenceExpression_1_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "referenceExpression_1_1_1")) return false;
    afterIndentifier(b, l + 1);
    return true;
  }

}
