// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GmlAfterIndentifier extends PsiElement {

  @NotNull
  List<GmlExpression> getExpressionList();

  @Nullable
  GmlFunctionArgList getFunctionArgList();

  @NotNull
  List<GmlIdentifierRef> getIdentifierRefList();

  @Nullable
  PsiElement getIdentifier();

  @Nullable
  PsiElement getNumber();

}
