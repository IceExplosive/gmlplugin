// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GmlConditionalExpression extends GmlExpression {

  @NotNull
  List<GmlExpression> getExpressionList();

}
