// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GmlConlessStatement extends PsiElement {

  @Nullable
  GmlConstDeclaration getConstDeclaration();

  @Nullable
  GmlDoStatement getDoStatement();

  @Nullable
  GmlEnumDeclaration getEnumDeclaration();

  @Nullable
  GmlExpression getExpression();

  @Nullable
  GmlForStatement getForStatement();

  @Nullable
  GmlGlobalvarDeclaration getGlobalvarDeclaration();

  @Nullable
  GmlIfStatement getIfStatement();

  @Nullable
  GmlRepeatStatement getRepeatStatement();

  @Nullable
  GmlReturnStatement getReturnStatement();

  @Nullable
  GmlStatementBlock getStatementBlock();

  @Nullable
  GmlSwitchStatement getSwitchStatement();

  @Nullable
  GmlVarDeclaration getVarDeclaration();

  @Nullable
  GmlWhileStatement getWhileStatement();

  @Nullable
  GmlWithStatement getWithStatement();

  @Nullable
  PsiElement getComment();

}
