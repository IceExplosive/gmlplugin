// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GmlIdentifierPlus extends PsiElement {

  @NotNull
  List<GmlAssignOp> getAssignOpList();

  @NotNull
  List<GmlEventType> getEventTypeList();

  @NotNull
  List<GmlExprOp> getExprOpList();

}
