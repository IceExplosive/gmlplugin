// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.navigation.ItemPresentation;

public interface GmlScriptFunctionDeclaration extends PsiElement {

  @Nullable
  GmlFunctionArgList getFunctionArgList();

  @NotNull
  GmlStatementBlock getStatementBlock();

  @NotNull
  PsiElement getIdentifier();

  ItemPresentation getPresentation();

  String getName();

}
