// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import gmlplugin.psi.impl.*;

public interface GmlTypes {

  IElementType AFTER_INDENTIFIER = new GmlElementType("AFTER_INDENTIFIER");
  IElementType ASSIGN_EXPRESSION = new GmlElementType("ASSIGN_EXPRESSION");
  IElementType ASSIGN_OP = new GmlElementType("ASSIGN_OP");
  IElementType CASE_STATEMENT = new GmlElementType("CASE_STATEMENT");
  IElementType COLLISION_EVENT = new GmlElementType("COLLISION_EVENT");
  IElementType COMPUTE_EXPRESSION = new GmlElementType("COMPUTE_EXPRESSION");
  IElementType CONDITIONAL_EXPRESSION = new GmlElementType("CONDITIONAL_EXPRESSION");
  IElementType CONLESS_STATEMENT = new GmlElementType("CONLESS_STATEMENT");
  IElementType CONST_DECLARATION = new GmlElementType("CONST_DECLARATION");
  IElementType DEFAULT_STATEMENT = new GmlElementType("DEFAULT_STATEMENT");
  IElementType DEF_COSNT = new GmlElementType("DEF_COSNT");
  IElementType DO_STATEMENT = new GmlElementType("DO_STATEMENT");
  IElementType ENUM_DECLARATION = new GmlElementType("ENUM_DECLARATION");
  IElementType ENUM_ENTRY_DECLARATION = new GmlElementType("ENUM_ENTRY_DECLARATION");
  IElementType EVENT_DECLARATION = new GmlElementType("EVENT_DECLARATION");
  IElementType EVENT_TYPE = new GmlElementType("EVENT_TYPE");
  IElementType EXPRESSION = new GmlElementType("EXPRESSION");
  IElementType EXPR_OP = new GmlElementType("EXPR_OP");
  IElementType FOR_STATEMENT = new GmlElementType("FOR_STATEMENT");
  IElementType FUNCTION_ARG_LIST = new GmlElementType("FUNCTION_ARG_LIST");
  IElementType FUNCTION_ARG_LIST_DECL = new GmlElementType("FUNCTION_ARG_LIST_DECL");
  IElementType FUNCTION_DECLARATION = new GmlElementType("FUNCTION_DECLARATION");
  IElementType FUNCTION_DECLARATION_NAME = new GmlElementType("FUNCTION_DECLARATION_NAME");
  IElementType FUNC_ARG_DECL = new GmlElementType("FUNC_ARG_DECL");
  IElementType FUNC_RETURNVAL = new GmlElementType("FUNC_RETURNVAL");
  IElementType FUNC_RETURN_NAME = new GmlElementType("FUNC_RETURN_NAME");
  IElementType GLOBALVAR_DECLARATION = new GmlElementType("GLOBALVAR_DECLARATION");
  IElementType GML_BUILTIN_CONSTANT = new GmlElementType("GML_BUILTIN_CONSTANT");
  IElementType GML_BUILTIN_CONSTANTS = new GmlElementType("GML_BUILTIN_CONSTANTS");
  IElementType GML_FUNCTIONS = new GmlElementType("GML_FUNCTIONS");
  IElementType GML_INSTANCE_VARIABLE = new GmlElementType("GML_INSTANCE_VARIABLE");
  IElementType GML_INSTANCE_VARIABLES = new GmlElementType("GML_INSTANCE_VARIABLES");
  IElementType GML_OBJECT = new GmlElementType("GML_OBJECT");
  IElementType GML_RESOURCE = new GmlElementType("GML_RESOURCE");
  IElementType GML_RESOURCES = new GmlElementType("GML_RESOURCES");
  IElementType GML_SCRIPT = new GmlElementType("GML_SCRIPT");
  IElementType IDENTIFIER_PLUS = new GmlElementType("IDENTIFIER_PLUS");
  IElementType IDENTIFIER_REF = new GmlElementType("IDENTIFIER_REF");
  IElementType IF_STATEMENT = new GmlElementType("IF_STATEMENT");
  IElementType INLINE_FUNCTION_DECLARATION = new GmlElementType("INLINE_FUNCTION_DECLARATION");
  IElementType LITERAL_EXPRESSION = new GmlElementType("LITERAL_EXPRESSION");
  IElementType PLUS_MINUS_EXPRESSION = new GmlElementType("PLUS_MINUS_EXPRESSION");
  IElementType PLUS_MINUS_OP = new GmlElementType("PLUS_MINUS_OP");
  IElementType PLUS_MINUS_REV_EXPRESSION = new GmlElementType("PLUS_MINUS_REV_EXPRESSION");
  IElementType REFERENCE_EXPRESSION = new GmlElementType("REFERENCE_EXPRESSION");
  IElementType REPEAT_STATEMENT = new GmlElementType("REPEAT_STATEMENT");
  IElementType RETURN_STATEMENT = new GmlElementType("RETURN_STATEMENT");
  IElementType ROUND_BRACKET_EXPRESSION = new GmlElementType("ROUND_BRACKET_EXPRESSION");
  IElementType SCRIPT_CALL = new GmlElementType("SCRIPT_CALL");
  IElementType SCRIPT_FUNCTION_DECLARATION = new GmlElementType("SCRIPT_FUNCTION_DECLARATION");
  IElementType SCRIPT_REF = new GmlElementType("SCRIPT_REF");
  IElementType STATEMENT = new GmlElementType("STATEMENT");
  IElementType STATEMENT_BLOCK = new GmlElementType("STATEMENT_BLOCK");
  IElementType STRUCT_DECLARATION = new GmlElementType("STRUCT_DECLARATION");
  IElementType STRUCT_FIELD_DECLARATION = new GmlElementType("STRUCT_FIELD_DECLARATION");
  IElementType SWITCH_STATEMENT = new GmlElementType("SWITCH_STATEMENT");
  IElementType TEST_EXPRESSION = new GmlElementType("TEST_EXPRESSION");
  IElementType TEST_OP = new GmlElementType("TEST_OP");
  IElementType VAR_DECLARATION = new GmlElementType("VAR_DECLARATION");
  IElementType WHILE_STATEMENT = new GmlElementType("WHILE_STATEMENT");
  IElementType WITH_STATEMENT = new GmlElementType("WITH_STATEMENT");
  IElementType ZERO_ELEVEN = new GmlElementType("ZERO_ELEVEN");

  IElementType ALARM_0 = new GmlTokenType("alarm_0");
  IElementType ALARM_1 = new GmlTokenType("alarm_1");
  IElementType ALARM_10 = new GmlTokenType("alarm_10");
  IElementType ALARM_11 = new GmlTokenType("alarm_11");
  IElementType ALARM_2 = new GmlTokenType("alarm_2");
  IElementType ALARM_3 = new GmlTokenType("alarm_3");
  IElementType ALARM_4 = new GmlTokenType("alarm_4");
  IElementType ALARM_5 = new GmlTokenType("alarm_5");
  IElementType ALARM_6 = new GmlTokenType("alarm_6");
  IElementType ALARM_7 = new GmlTokenType("alarm_7");
  IElementType ALARM_8 = new GmlTokenType("alarm_8");
  IElementType ALARM_9 = new GmlTokenType("alarm_9");
  IElementType ALL = new GmlTokenType("all");
  IElementType AND = new GmlTokenType("&");
  IElementType ANDAND = new GmlTokenType("&&");
  IElementType ANDEQ = new GmlTokenType("&=");
  IElementType ANIMATION_END = new GmlTokenType("animation_end");
  IElementType ANIMATION_EVENT = new GmlTokenType("animation_event");
  IElementType ANIMATION_UPDATE = new GmlTokenType("animation_update");
  IElementType ARGUMENT = new GmlTokenType("argument");
  IElementType ARGUMENT0 = new GmlTokenType("argument0");
  IElementType ARGUMENT1 = new GmlTokenType("argument1");
  IElementType ARGUMENT10 = new GmlTokenType("argument10");
  IElementType ARGUMENT11 = new GmlTokenType("argument11");
  IElementType ARGUMENT12 = new GmlTokenType("argument12");
  IElementType ARGUMENT13 = new GmlTokenType("argument13");
  IElementType ARGUMENT14 = new GmlTokenType("argument14");
  IElementType ARGUMENT15 = new GmlTokenType("argument15");
  IElementType ARGUMENT2 = new GmlTokenType("argument2");
  IElementType ARGUMENT3 = new GmlTokenType("argument3");
  IElementType ARGUMENT4 = new GmlTokenType("argument4");
  IElementType ARGUMENT5 = new GmlTokenType("argument5");
  IElementType ARGUMENT6 = new GmlTokenType("argument6");
  IElementType ARGUMENT7 = new GmlTokenType("argument7");
  IElementType ARGUMENT8 = new GmlTokenType("argument8");
  IElementType ARGUMENT9 = new GmlTokenType("argument9");
  IElementType ARGUMENT_COUNT = new GmlTokenType("argument_count");
  IElementType ASYNC_AUDIO_PLAYBACK = new GmlTokenType("async_audio_playback");
  IElementType ASYNC_AUDIO_RECORDING = new GmlTokenType("async_audio_recording");
  IElementType ASYNC_CLOUD = new GmlTokenType("async_cloud");
  IElementType ASYNC_DIALOG = new GmlTokenType("async_dialog");
  IElementType ASYNC_HTTP = new GmlTokenType("async_http");
  IElementType ASYNC_IMAGE_LOADED = new GmlTokenType("async_image_loaded");
  IElementType ASYNC_IN_APP_PURCHASE = new GmlTokenType("async_in_app_purchase");
  IElementType ASYNC_NETWORKING = new GmlTokenType("async_networking");
  IElementType ASYNC_PUSH_NOTIFICATION = new GmlTokenType("async_push_notification");
  IElementType ASYNC_SAVE_LOAD = new GmlTokenType("async_save_load");
  IElementType ASYNC_SOCIAL = new GmlTokenType("async_social");
  IElementType ASYNC_STEAM = new GmlTokenType("async_steam");
  IElementType ASYNC_SYSTEM = new GmlTokenType("async_system");
  IElementType BAD_CHARACTER = new GmlTokenType("bad_character");
  IElementType BREAK = new GmlTokenType("break");
  IElementType BUILTIN_CONSTANTS = new GmlTokenType("builtin_constants");
  IElementType CASE = new GmlTokenType("case");
  IElementType CHARACTER = new GmlTokenType("character");
  IElementType CLEAN_UP = new GmlTokenType("clean_up");
  IElementType COLLISION = new GmlTokenType("collision");
  IElementType COLON = new GmlTokenType(":");
  IElementType COMMA = new GmlTokenType(",");
  IElementType COMMENT = new GmlTokenType("comment");
  IElementType COMP = new GmlTokenType("~");
  IElementType COMPEQ = new GmlTokenType("~=");
  IElementType CONTINUE = new GmlTokenType("continue");
  IElementType CREATE = new GmlTokenType("create");
  IElementType DEFAULT = new GmlTokenType("default");
  IElementType DESTROY = new GmlTokenType("destroy");
  IElementType DIV = new GmlTokenType("/");
  IElementType DIVEQ = new GmlTokenType("/=");
  IElementType DO = new GmlTokenType("do");
  IElementType DOT = new GmlTokenType(".");
  IElementType DRAW = new GmlTokenType("draw");
  IElementType DRAW_BEGIN = new GmlTokenType("draw_begin");
  IElementType DRAW_END = new GmlTokenType("draw_end");
  IElementType DRAW_GUI = new GmlTokenType("draw_GUI");
  IElementType DRAW_GUI_BEGIN = new GmlTokenType("draw_GUI_begin");
  IElementType DRAW_GUI_END = new GmlTokenType("draw_GUI_end");
  IElementType ELSE = new GmlTokenType("else");
  IElementType ENUM = new GmlTokenType("enum");
  IElementType EQ = new GmlTokenType("=");
  IElementType EQEQ = new GmlTokenType("==");
  IElementType EVENT = new GmlTokenType("event");
  IElementType EXIT = new GmlTokenType("exit");
  IElementType FALSE = new GmlTokenType("false");
  IElementType FOR = new GmlTokenType("for");
  IElementType FUNCTION = new GmlTokenType("function");
  IElementType FUNCTIONS = new GmlTokenType("functions");
  IElementType GAME_END = new GmlTokenType("game_end");
  IElementType GAME_START = new GmlTokenType("game_start");
  IElementType GESTURE_DOUBLE_TAP = new GmlTokenType("gesture_double_tap");
  IElementType GESTURE_DRAGGING = new GmlTokenType("gesture_dragging");
  IElementType GESTURE_DRAG_END = new GmlTokenType("gesture_drag_end");
  IElementType GESTURE_DRAG_START = new GmlTokenType("gesture_drag_start");
  IElementType GESTURE_FLICK = new GmlTokenType("gesture_flick");
  IElementType GESTURE_PINCH_END = new GmlTokenType("gesture_pinch_end");
  IElementType GESTURE_PINCH_IN = new GmlTokenType("gesture_pinch_in");
  IElementType GESTURE_PINCH_OUT = new GmlTokenType("gesture_pinch_out");
  IElementType GESTURE_PINCH_START = new GmlTokenType("gesture_pinch_start");
  IElementType GESTURE_ROTATE_END = new GmlTokenType("gesture_rotate_end");
  IElementType GESTURE_ROTATE_START = new GmlTokenType("gesture_rotate_start");
  IElementType GESTURE_ROTATING = new GmlTokenType("gesture_rotating");
  IElementType GESTURE_TAP = new GmlTokenType("gesture_tap");
  IElementType GLOBALVAR = new GmlTokenType("globalvar");
  IElementType GLOBAL_GESTURE_DOUBLE_TAP = new GmlTokenType("global_gesture_double_tap");
  IElementType GLOBAL_GESTURE_DRAGGING = new GmlTokenType("global_gesture_dragging");
  IElementType GLOBAL_GESTURE_DRAG_END = new GmlTokenType("global_gesture_drag_end");
  IElementType GLOBAL_GESTURE_DRAG_START = new GmlTokenType("global_gesture_drag_start");
  IElementType GLOBAL_GESTURE_FLICK = new GmlTokenType("global_gesture_flick");
  IElementType GLOBAL_GESTURE_PINCH_END = new GmlTokenType("global_gesture_pinch_end");
  IElementType GLOBAL_GESTURE_PINCH_IN = new GmlTokenType("global_gesture_pinch_in");
  IElementType GLOBAL_GESTURE_PINCH_OUT = new GmlTokenType("global_gesture_pinch_out");
  IElementType GLOBAL_GESTURE_PINCH_START = new GmlTokenType("global_gesture_pinch_start");
  IElementType GLOBAL_GESTURE_ROTATE_END = new GmlTokenType("global_gesture_rotate_end");
  IElementType GLOBAL_GESTURE_ROTATE_START = new GmlTokenType("global_gesture_rotate_start");
  IElementType GLOBAL_GESTURE_ROTATING = new GmlTokenType("global_gesture_rotating");
  IElementType GLOBAL_GESTURE_TAP = new GmlTokenType("global_gesture_tap");
  IElementType GLOBAL_IDENTIFIER = new GmlTokenType("__global__");
  IElementType GLOBAL_MOUSE_LEFT_DOWN = new GmlTokenType("global_mouse_left_down");
  IElementType GLOBAL_MOUSE_LEFT_PRESSED = new GmlTokenType("global_mouse_left_pressed");
  IElementType GLOBAL_MOUSE_LEFT_RELEASE = new GmlTokenType("global_mouse_left_release");
  IElementType GLOBAL_MOUSE_MIDDLE_DOWN = new GmlTokenType("global_mouse_middle_down");
  IElementType GLOBAL_MOUSE_MIDDLE_PRESSED = new GmlTokenType("global_mouse_middle_pressed");
  IElementType GLOBAL_MOUSE_MIDDLE_RELEASE = new GmlTokenType("global_mouse_middle_release");
  IElementType GLOBAL_MOUSE_RIGHT_DOWN = new GmlTokenType("global_mouse_right_down");
  IElementType GLOBAL_MOUSE_RIGHT_PRESSED = new GmlTokenType("global_mouse_right_pressed");
  IElementType GLOBAL_MOUSE_RIGHT_RELEASE = new GmlTokenType("global_mouse_right_release");
  IElementType GT = new GmlTokenType(">=");
  IElementType GTEQ = new GmlTokenType("gteq");
  IElementType IDENTIFIER = new GmlTokenType("identifier");
  IElementType IF = new GmlTokenType("if");
  IElementType INSTANCE_VARIABLES = new GmlTokenType("instance_variables");
  IElementType INTERSECT_BOUNDARY = new GmlTokenType("intersect_boundary");
  IElementType INTERSECT_VIEW_0_BOUNDARY = new GmlTokenType("intersect_view_0_boundary");
  IElementType INTERSECT_VIEW_1_BOUNDARY = new GmlTokenType("intersect_view_1_boundary");
  IElementType INTERSECT_VIEW_2_BOUNDARY = new GmlTokenType("intersect_view_2_boundary");
  IElementType INTERSECT_VIEW_3_BOUNDARY = new GmlTokenType("intersect_view_3_boundary");
  IElementType INTERSECT_VIEW_4_BOUNDARY = new GmlTokenType("intersect_view_4_boundary");
  IElementType INTERSECT_VIEW_5_BOUNDARY = new GmlTokenType("intersect_view_5_boundary");
  IElementType INTERSECT_VIEW_6_BOUNDARY = new GmlTokenType("intersect_view_6_boundary");
  IElementType INTERSECT_VIEW_7_BOUNDARY = new GmlTokenType("intersect_view_7_boundary");
  IElementType KEY_0_DOWN = new GmlTokenType("key_0_down");
  IElementType KEY_0_PRESSED = new GmlTokenType("key_0_pressed");
  IElementType KEY_0_RELEASED = new GmlTokenType("key_0_released");
  IElementType KEY_1_DOWN = new GmlTokenType("key_1_down");
  IElementType KEY_1_PRESSED = new GmlTokenType("key_1_pressed");
  IElementType KEY_1_RELEASED = new GmlTokenType("key_1_released");
  IElementType KEY_2_DOWN = new GmlTokenType("key_2_down");
  IElementType KEY_2_PRESSED = new GmlTokenType("key_2_pressed");
  IElementType KEY_2_RELEASED = new GmlTokenType("key_2_released");
  IElementType KEY_3_DOWN = new GmlTokenType("key_3_down");
  IElementType KEY_3_PRESSED = new GmlTokenType("key_3_pressed");
  IElementType KEY_3_RELEASED = new GmlTokenType("key_3_released");
  IElementType KEY_3_UP = new GmlTokenType("key_3_up");
  IElementType KEY_4_DOWN = new GmlTokenType("key_4_down");
  IElementType KEY_4_PRESSED = new GmlTokenType("key_4_pressed");
  IElementType KEY_4_RELEASED = new GmlTokenType("key_4_released");
  IElementType KEY_5_DOWN = new GmlTokenType("key_5_down");
  IElementType KEY_5_PRESSED = new GmlTokenType("key_5_pressed");
  IElementType KEY_5_RELEASED = new GmlTokenType("key_5_released");
  IElementType KEY_6_DOWN = new GmlTokenType("key_6_down");
  IElementType KEY_6_PRESSED = new GmlTokenType("key_6_pressed");
  IElementType KEY_6_RELEASED = new GmlTokenType("key_6_released");
  IElementType KEY_6_UP = new GmlTokenType("key_6_up");
  IElementType KEY_7_DOWN = new GmlTokenType("key_7_down");
  IElementType KEY_7_PRESSED = new GmlTokenType("key_7_pressed");
  IElementType KEY_7_RELEASED = new GmlTokenType("key_7_released");
  IElementType KEY_8_DOWN = new GmlTokenType("key_8_down");
  IElementType KEY_8_PRESSED = new GmlTokenType("key_8_pressed");
  IElementType KEY_8_RELEASED = new GmlTokenType("key_8_released");
  IElementType KEY_9_DOWN = new GmlTokenType("key_9_down");
  IElementType KEY_9_PRESSED = new GmlTokenType("key_9_pressed");
  IElementType KEY_9_RELEASED = new GmlTokenType("key_9_released");
  IElementType KEY_9_UP = new GmlTokenType("key_9_up");
  IElementType KEY_ALT_DOWN = new GmlTokenType("key_alt_down");
  IElementType KEY_ALT_PRESSED = new GmlTokenType("key_alt_pressed");
  IElementType KEY_ALT_RELEASED = new GmlTokenType("key_alt_released");
  IElementType KEY_ANY_DOWN = new GmlTokenType("key_any_down");
  IElementType KEY_ANY_PRESSED = new GmlTokenType("key_any_pressed");
  IElementType KEY_ANY_RELEASED = new GmlTokenType("key_any_released");
  IElementType KEY_A_DOWN = new GmlTokenType("key_A_down");
  IElementType KEY_A_PRESSED = new GmlTokenType("key_A_pressed");
  IElementType KEY_A_RELEASED = new GmlTokenType("key_A_released");
  IElementType KEY_BACKSPACE_DOWN = new GmlTokenType("key_backspace_down");
  IElementType KEY_BACKSPACE_PRESSED = new GmlTokenType("key_backspace_pressed");
  IElementType KEY_BACKSPACE_RELEASED = new GmlTokenType("key_backspace_released");
  IElementType KEY_B_DOWN = new GmlTokenType("key_B_down");
  IElementType KEY_B_PRESSED = new GmlTokenType("key_B_pressed");
  IElementType KEY_B_RELEASED = new GmlTokenType("key_B_released");
  IElementType KEY_CONTROL_DOWN = new GmlTokenType("key_control_down");
  IElementType KEY_CONTROL_PRESSED = new GmlTokenType("key_control_pressed");
  IElementType KEY_CONTROL_RELEASED = new GmlTokenType("key_control_released");
  IElementType KEY_C_DOWN = new GmlTokenType("key_C_down");
  IElementType KEY_C_PRESSED = new GmlTokenType("key_C_pressed");
  IElementType KEY_C_RELEASED = new GmlTokenType("key_C_released");
  IElementType KEY_C_UP = new GmlTokenType("key_C_up");
  IElementType KEY_DELETE_DOWN = new GmlTokenType("key_delete_down");
  IElementType KEY_DELETE_PRESSED = new GmlTokenType("key_delete_pressed");
  IElementType KEY_DELETE_RELEASED = new GmlTokenType("key_delete_released");
  IElementType KEY_DOWN_DOWN = new GmlTokenType("key_down_down");
  IElementType KEY_DOWN_PRESSED = new GmlTokenType("key_down_pressed");
  IElementType KEY_DOWN_RELEASED = new GmlTokenType("key_down_released");
  IElementType KEY_D_DOWN = new GmlTokenType("key_D_down");
  IElementType KEY_D_PRESSED = new GmlTokenType("key_D_pressed");
  IElementType KEY_D_RELEASED = new GmlTokenType("key_D_released");
  IElementType KEY_END_DOWN = new GmlTokenType("key_end_down");
  IElementType KEY_END_PRESSED = new GmlTokenType("key_end_pressed");
  IElementType KEY_END_RELEASED = new GmlTokenType("key_end_released");
  IElementType KEY_ENTER_DOWN = new GmlTokenType("key_enter_down");
  IElementType KEY_ENTER_PRESSED = new GmlTokenType("key_enter_pressed");
  IElementType KEY_ENTER_RELEASED = new GmlTokenType("key_enter_released");
  IElementType KEY_ESCAPE_DOWN = new GmlTokenType("key_escape_down");
  IElementType KEY_ESCAPE_PRESSED = new GmlTokenType("key_escape_pressed");
  IElementType KEY_ESCAPE_RELEASED = new GmlTokenType("key_escape_released");
  IElementType KEY_E_DOWN = new GmlTokenType("key_E_down");
  IElementType KEY_E_PRESSED = new GmlTokenType("key_E_pressed");
  IElementType KEY_E_RELEASED = new GmlTokenType("key_E_released");
  IElementType KEY_F10_DOWN = new GmlTokenType("key_F10_down");
  IElementType KEY_F10_PRESSED = new GmlTokenType("key_F10_pressed");
  IElementType KEY_F10_RELEASED = new GmlTokenType("key_F10_released");
  IElementType KEY_F11_DOWN = new GmlTokenType("key_F11_down");
  IElementType KEY_F11_PRESSED = new GmlTokenType("key_F11_pressed");
  IElementType KEY_F11_RELEASED = new GmlTokenType("key_F11_released");
  IElementType KEY_F12_DOWN = new GmlTokenType("key_F12_down");
  IElementType KEY_F12_PRESSED = new GmlTokenType("key_F12_pressed");
  IElementType KEY_F12_RELEASED = new GmlTokenType("key_F12_released");
  IElementType KEY_F1_DOWN = new GmlTokenType("key_F1_down");
  IElementType KEY_F1_PRESSED = new GmlTokenType("key_F1_pressed");
  IElementType KEY_F1_RELEASED = new GmlTokenType("key_F1_released");
  IElementType KEY_F1_UP = new GmlTokenType("key_F1_up");
  IElementType KEY_F2_DOWN = new GmlTokenType("key_F2_down");
  IElementType KEY_F2_PRESSED = new GmlTokenType("key_F2_pressed");
  IElementType KEY_F2_RELEASED = new GmlTokenType("key_F2_released");
  IElementType KEY_F3_DOWN = new GmlTokenType("key_F3_down");
  IElementType KEY_F3_PRESSED = new GmlTokenType("key_F3_pressed");
  IElementType KEY_F3_RELEASED = new GmlTokenType("key_F3_released");
  IElementType KEY_F4_DOWN = new GmlTokenType("key_F4_down");
  IElementType KEY_F4_PRESSED = new GmlTokenType("key_F4_pressed");
  IElementType KEY_F4_RELEASED = new GmlTokenType("key_F4_released");
  IElementType KEY_F5_DOWN = new GmlTokenType("key_F5_down");
  IElementType KEY_F5_PRESSED = new GmlTokenType("key_F5_pressed");
  IElementType KEY_F5_RELEASED = new GmlTokenType("key_F5_released");
  IElementType KEY_F6_DOWN = new GmlTokenType("key_F6_down");
  IElementType KEY_F6_PRESSED = new GmlTokenType("key_F6_pressed");
  IElementType KEY_F6_RELEASED = new GmlTokenType("key_F6_released");
  IElementType KEY_F7_DOWN = new GmlTokenType("key_F7_down");
  IElementType KEY_F7_PRESSED = new GmlTokenType("key_F7_pressed");
  IElementType KEY_F7_RELEASED = new GmlTokenType("key_F7_released");
  IElementType KEY_F8_DOWN = new GmlTokenType("key_F8_down");
  IElementType KEY_F8_PRESSED = new GmlTokenType("key_F8_pressed");
  IElementType KEY_F8_RELEASED = new GmlTokenType("key_F8_released");
  IElementType KEY_F9_DOWN = new GmlTokenType("key_F9_down");
  IElementType KEY_F9_PRESSED = new GmlTokenType("key_F9_pressed");
  IElementType KEY_F9_RELEASED = new GmlTokenType("key_F9_released");
  IElementType KEY_F_DOWN = new GmlTokenType("key_F_down");
  IElementType KEY_F_PRESSED = new GmlTokenType("key_F_pressed");
  IElementType KEY_F_RELEASED = new GmlTokenType("key_F_released");
  IElementType KEY_F_UP = new GmlTokenType("key_F_up");
  IElementType KEY_G_DOWN = new GmlTokenType("key_G_down");
  IElementType KEY_G_PRESSED = new GmlTokenType("key_G_pressed");
  IElementType KEY_G_RELEASED = new GmlTokenType("key_G_released");
  IElementType KEY_HOME_DOWN = new GmlTokenType("key_home_down");
  IElementType KEY_HOME_PRESSED = new GmlTokenType("key_home_pressed");
  IElementType KEY_HOME_RELEASED = new GmlTokenType("key_home_released");
  IElementType KEY_H_DOWN = new GmlTokenType("key_H_down");
  IElementType KEY_H_PRESSED = new GmlTokenType("key_H_pressed");
  IElementType KEY_H_RELEASED = new GmlTokenType("key_H_released");
  IElementType KEY_INSERT_DOWN = new GmlTokenType("key_insert_down");
  IElementType KEY_INSERT_PRESSED = new GmlTokenType("key_insert_pressed");
  IElementType KEY_INSERT_RELEASED = new GmlTokenType("key_insert_released");
  IElementType KEY_I_DOWN = new GmlTokenType("key_I_down");
  IElementType KEY_I_PRESSED = new GmlTokenType("key_I_pressed");
  IElementType KEY_I_RELEASED = new GmlTokenType("key_I_released");
  IElementType KEY_I_UP = new GmlTokenType("key_I_up");
  IElementType KEY_J_DOWN = new GmlTokenType("key_J_down");
  IElementType KEY_J_PRESSED = new GmlTokenType("key_J_pressed");
  IElementType KEY_J_RELEASED = new GmlTokenType("key_J_released");
  IElementType KEY_KEYPAD_0_DOWN = new GmlTokenType("key_keypad_0_down");
  IElementType KEY_KEYPAD_0_PRESSED = new GmlTokenType("key_keypad_0_pressed");
  IElementType KEY_KEYPAD_0_RELEASED = new GmlTokenType("key_keypad_0_released");
  IElementType KEY_KEYPAD_1_DOWN = new GmlTokenType("key_keypad_1_down");
  IElementType KEY_KEYPAD_1_PRESSED = new GmlTokenType("key_keypad_1_pressed");
  IElementType KEY_KEYPAD_1_RELEASED = new GmlTokenType("key_keypad_1_released");
  IElementType KEY_KEYPAD_2_DOWN = new GmlTokenType("key_keypad_2_down");
  IElementType KEY_KEYPAD_2_PRESSED = new GmlTokenType("key_keypad_2_pressed");
  IElementType KEY_KEYPAD_2_RELEASED = new GmlTokenType("key_keypad_2_released");
  IElementType KEY_KEYPAD_3_DOWN = new GmlTokenType("key_keypad_3_down");
  IElementType KEY_KEYPAD_3_PRESSED = new GmlTokenType("key_keypad_3_pressed");
  IElementType KEY_KEYPAD_3_RELEASED = new GmlTokenType("key_keypad_3_released");
  IElementType KEY_KEYPAD_4_DOWN = new GmlTokenType("key_keypad_4_down");
  IElementType KEY_KEYPAD_4_PRESSED = new GmlTokenType("key_keypad_4_pressed");
  IElementType KEY_KEYPAD_4_RELEASED = new GmlTokenType("key_keypad_4_released");
  IElementType KEY_KEYPAD_5_DOWN = new GmlTokenType("key_keypad_5_down");
  IElementType KEY_KEYPAD_5_PRESSED = new GmlTokenType("key_keypad_5_pressed");
  IElementType KEY_KEYPAD_5_RELEASED = new GmlTokenType("key_keypad_5_released");
  IElementType KEY_KEYPAD_6_DOWN = new GmlTokenType("key_keypad_6_down");
  IElementType KEY_KEYPAD_6_PRESSED = new GmlTokenType("key_keypad_6_pressed");
  IElementType KEY_KEYPAD_6_RELEASED = new GmlTokenType("key_keypad_6_released");
  IElementType KEY_KEYPAD_7_DOWN = new GmlTokenType("key_keypad_7_down");
  IElementType KEY_KEYPAD_7_PRESSED = new GmlTokenType("key_keypad_7_pressed");
  IElementType KEY_KEYPAD_7_RELEASED = new GmlTokenType("key_keypad_7_released");
  IElementType KEY_KEYPAD_8_DOWN = new GmlTokenType("key_keypad_8_down");
  IElementType KEY_KEYPAD_8_PRESSED = new GmlTokenType("key_keypad_8_pressed");
  IElementType KEY_KEYPAD_8_RELEASED = new GmlTokenType("key_keypad_8_released");
  IElementType KEY_KEYPAD_9_DOWN = new GmlTokenType("key_keypad_9_down");
  IElementType KEY_KEYPAD_9_PRESSED = new GmlTokenType("key_keypad_9_pressed");
  IElementType KEY_KEYPAD_9_RELEASED = new GmlTokenType("key_keypad_9_released");
  IElementType KEY_KEYPAD_DOT_DOWN = new GmlTokenType("key_keypad_dot_down");
  IElementType KEY_KEYPAD_DOT_PRESSED = new GmlTokenType("key_keypad_dot_pressed");
  IElementType KEY_KEYPAD_DOT_RELEASED = new GmlTokenType("key_keypad_dot_released");
  IElementType KEY_KEYPAD_MINUS_DOWN = new GmlTokenType("key_keypad_minus_down");
  IElementType KEY_KEYPAD_MINUS_PRESSED = new GmlTokenType("key_keypad_minus_pressed");
  IElementType KEY_KEYPAD_MINUS_RELEASED = new GmlTokenType("key_keypad_minus_released");
  IElementType KEY_KEYPAD_MULT_DOWN = new GmlTokenType("key_keypad_mult_down");
  IElementType KEY_KEYPAD_MULT_PRESSED = new GmlTokenType("key_keypad_mult_pressed");
  IElementType KEY_KEYPAD_MULT_RELEASED = new GmlTokenType("key_keypad_mult_released");
  IElementType KEY_KEYPAD_PLUS_DOWN = new GmlTokenType("key_keypad_plus_down");
  IElementType KEY_KEYPAD_PLUS_PRESSED = new GmlTokenType("key_keypad_plus_pressed");
  IElementType KEY_KEYPAD_PLUS_RELEASED = new GmlTokenType("key_keypad_plus_released");
  IElementType KEY_KEYPAD_SLASH_DOWN = new GmlTokenType("key_keypad_slash_down");
  IElementType KEY_KEYPAD_SLASH_PRESSED = new GmlTokenType("key_keypad_slash_pressed");
  IElementType KEY_KEYPAD_SLASH_RELEASED = new GmlTokenType("key_keypad_slash_released");
  IElementType KEY_K_DOWN = new GmlTokenType("key_K_down");
  IElementType KEY_K_PRESSED = new GmlTokenType("key_K_pressed");
  IElementType KEY_K_RELEASED = new GmlTokenType("key_K_released");
  IElementType KEY_LEFT_DOWN = new GmlTokenType("key_left_down");
  IElementType KEY_LEFT_PRESSED = new GmlTokenType("key_left_pressed");
  IElementType KEY_LEFT_RELEASED = new GmlTokenType("key_left_released");
  IElementType KEY_L_DOWN = new GmlTokenType("key_L_down");
  IElementType KEY_L_PRESSED = new GmlTokenType("key_L_pressed");
  IElementType KEY_L_RELEASED = new GmlTokenType("key_L_released");
  IElementType KEY_L_UP = new GmlTokenType("key_L_up");
  IElementType KEY_M_DOWN = new GmlTokenType("key_M_down");
  IElementType KEY_M_PRESSED = new GmlTokenType("key_M_pressed");
  IElementType KEY_M_RELEASED = new GmlTokenType("key_M_released");
  IElementType KEY_NO_DOWN = new GmlTokenType("key_no_down");
  IElementType KEY_NO_PRESSED = new GmlTokenType("key_no_pressed");
  IElementType KEY_NO_RELEASED = new GmlTokenType("key_no_released");
  IElementType KEY_N_DOWN = new GmlTokenType("key_N_down");
  IElementType KEY_N_PRESSED = new GmlTokenType("key_N_pressed");
  IElementType KEY_N_RELEASED = new GmlTokenType("key_N_released");
  IElementType KEY_O_DOWN = new GmlTokenType("key_O_down");
  IElementType KEY_O_PRESSED = new GmlTokenType("key_O_pressed");
  IElementType KEY_O_RELEASED = new GmlTokenType("key_O_released");
  IElementType KEY_O_UP = new GmlTokenType("key_O_up");
  IElementType KEY_PAGE_DOWN_DOWN = new GmlTokenType("key_page_down_down");
  IElementType KEY_PAGE_DOWN_PRESSED = new GmlTokenType("key_page_down_pressed");
  IElementType KEY_PAGE_DOWN_RELEASED = new GmlTokenType("key_page_down_released");
  IElementType KEY_PAGE_UP_DOWN = new GmlTokenType("key_page_up_down");
  IElementType KEY_PAGE_UP_PRESSED = new GmlTokenType("key_page_up_pressed");
  IElementType KEY_PAGE_UP_RELEASED = new GmlTokenType("key_page_up_released");
  IElementType KEY_P_DOWN = new GmlTokenType("key_P_down");
  IElementType KEY_P_PRESSED = new GmlTokenType("key_P_pressed");
  IElementType KEY_P_RELEASED = new GmlTokenType("key_P_released");
  IElementType KEY_Q_DOWN = new GmlTokenType("key_Q_down");
  IElementType KEY_Q_PRESSED = new GmlTokenType("key_Q_pressed");
  IElementType KEY_Q_RELEASED = new GmlTokenType("key_Q_released");
  IElementType KEY_RIGHT_DOWN = new GmlTokenType("key_right_down");
  IElementType KEY_RIGHT_PRESSED = new GmlTokenType("key_right_pressed");
  IElementType KEY_RIGHT_RELEASED = new GmlTokenType("key_right_released");
  IElementType KEY_R_DOWN = new GmlTokenType("key_R_down");
  IElementType KEY_R_PRESSED = new GmlTokenType("key_R_pressed");
  IElementType KEY_R_RELEASED = new GmlTokenType("key_R_released");
  IElementType KEY_R_UP = new GmlTokenType("key_R_up");
  IElementType KEY_SHIFT_DOWN = new GmlTokenType("key_shift_down");
  IElementType KEY_SHIFT_PRESSED = new GmlTokenType("key_shift_pressed");
  IElementType KEY_SHIFT_RELEASED = new GmlTokenType("key_shift_released");
  IElementType KEY_SPACE_DOWN = new GmlTokenType("key_space_down");
  IElementType KEY_SPACE_PRESSED = new GmlTokenType("key_space_pressed");
  IElementType KEY_SPACE_RELEASED = new GmlTokenType("key_space_released");
  IElementType KEY_S_DOWN = new GmlTokenType("key_S_down");
  IElementType KEY_S_PRESSED = new GmlTokenType("key_S_pressed");
  IElementType KEY_S_RELEASED = new GmlTokenType("key_S_released");
  IElementType KEY_T_DOWN = new GmlTokenType("key_T_down");
  IElementType KEY_T_PRESSED = new GmlTokenType("key_T_pressed");
  IElementType KEY_T_RELEASED = new GmlTokenType("key_T_released");
  IElementType KEY_UP_DOWN = new GmlTokenType("key_up_down");
  IElementType KEY_UP_PRESSED = new GmlTokenType("key_up_pressed");
  IElementType KEY_UP_RELEASED = new GmlTokenType("key_up_released");
  IElementType KEY_U_DOWN = new GmlTokenType("key_U_down");
  IElementType KEY_U_PRESSED = new GmlTokenType("key_U_pressed");
  IElementType KEY_U_RELEASED = new GmlTokenType("key_U_released");
  IElementType KEY_U_UP = new GmlTokenType("key_U_up");
  IElementType KEY_V_DOWN = new GmlTokenType("key_V_down");
  IElementType KEY_V_PRESSED = new GmlTokenType("key_V_pressed");
  IElementType KEY_V_RELEASED = new GmlTokenType("key_V_released");
  IElementType KEY_W_DOWN = new GmlTokenType("key_W_down");
  IElementType KEY_W_PRESSED = new GmlTokenType("key_W_pressed");
  IElementType KEY_W_RELEASED = new GmlTokenType("key_W_released");
  IElementType KEY_X_DOWN = new GmlTokenType("key_X_down");
  IElementType KEY_X_PRESSED = new GmlTokenType("key_X_pressed");
  IElementType KEY_X_RELEASED = new GmlTokenType("key_X_released");
  IElementType KEY_X_UP = new GmlTokenType("key_X_up");
  IElementType KEY_Y_DOWN = new GmlTokenType("key_Y_down");
  IElementType KEY_Y_PRESSED = new GmlTokenType("key_Y_pressed");
  IElementType KEY_Y_RELEASED = new GmlTokenType("key_Y_released");
  IElementType KEY_Z_DOWN = new GmlTokenType("key_Z_down");
  IElementType KEY_Z_PRESSED = new GmlTokenType("key_Z_pressed");
  IElementType KEY_Z_RELEASED = new GmlTokenType("key_Z_released");
  IElementType LCBR = new GmlTokenType("{");
  IElementType LOCAL_IDENTIFIER = new GmlTokenType("__local__");
  IElementType LRBR = new GmlTokenType("(");
  IElementType LSBR = new GmlTokenType("[");
  IElementType LSHIFT = new GmlTokenType("<<");
  IElementType LT = new GmlTokenType("<");
  IElementType LTEQ = new GmlTokenType("<=");
  IElementType MACRO = new GmlTokenType("#macro");
  IElementType MINUS = new GmlTokenType("-");
  IElementType MINUSEQ = new GmlTokenType("-=");
  IElementType MINUSMINUS = new GmlTokenType("--");
  IElementType MOD = new GmlTokenType("%");
  IElementType MODEQ = new GmlTokenType("%=");
  IElementType MOUSE_ENTER = new GmlTokenType("mouse_enter");
  IElementType MOUSE_LEAVE = new GmlTokenType("mouse_leave");
  IElementType MOUSE_LEFT_DOWN = new GmlTokenType("mouse_left_down");
  IElementType MOUSE_LEFT_PRESSED = new GmlTokenType("mouse_left_pressed");
  IElementType MOUSE_LEFT_RELEASE = new GmlTokenType("mouse_left_release");
  IElementType MOUSE_MIDDLE_DOWN = new GmlTokenType("mouse_middle_down");
  IElementType MOUSE_MIDDLE_PRESSED = new GmlTokenType("mouse_middle_pressed");
  IElementType MOUSE_MIDDLE_RELEASE = new GmlTokenType("mouse_middle_release");
  IElementType MOUSE_RIGHT_DOWN = new GmlTokenType("mouse_right_down");
  IElementType MOUSE_RIGHT_PRESSED = new GmlTokenType("mouse_right_pressed");
  IElementType MOUSE_RIGHT_RELEASE = new GmlTokenType("mouse_right_release");
  IElementType MOUSE_WHEEL_DOWN = new GmlTokenType("mouse_wheel_down");
  IElementType MOUSE_WHEEL_UP = new GmlTokenType("mouse_wheel_up");
  IElementType MULT = new GmlTokenType("*");
  IElementType MULTEQ = new GmlTokenType("*=");
  IElementType NOONE = new GmlTokenType("noone");
  IElementType NOT = new GmlTokenType("!");
  IElementType NOTEQ = new GmlTokenType("!=");
  IElementType NO_MOUSE = new GmlTokenType("no_mouse");
  IElementType NUMBER = new GmlTokenType("number");
  IElementType OR = new GmlTokenType("|");
  IElementType OREQ = new GmlTokenType("|=");
  IElementType OROR = new GmlTokenType("||");
  IElementType OTHER = new GmlTokenType("other");
  IElementType OUTSIDE_ROOM = new GmlTokenType("outside_room");
  IElementType OUTSIDE_VIEW_0 = new GmlTokenType("outside_view_0");
  IElementType OUTSIDE_VIEW_1 = new GmlTokenType("outside_view_1");
  IElementType OUTSIDE_VIEW_2 = new GmlTokenType("outside_view_2");
  IElementType OUTSIDE_VIEW_3 = new GmlTokenType("outside_view_3");
  IElementType OUTSIDE_VIEW_4 = new GmlTokenType("outside_view_4");
  IElementType OUTSIDE_VIEW_5 = new GmlTokenType("outside_view_5");
  IElementType OUTSIDE_VIEW_6 = new GmlTokenType("outside_view_6");
  IElementType OUTSIDE_VIEW_7 = new GmlTokenType("outside_view_7");
  IElementType PATH_ENDED = new GmlTokenType("path_ended");
  IElementType PI = new GmlTokenType("pi");
  IElementType PLUS = new GmlTokenType("+");
  IElementType PLUSEQ = new GmlTokenType("+=");
  IElementType PLUSPLUS = new GmlTokenType("++");
  IElementType POST_DRAW = new GmlTokenType("post_draw");
  IElementType PRE_DRAW = new GmlTokenType("pre_draw");
  IElementType QUESTION = new GmlTokenType("?");
  IElementType RCBR = new GmlTokenType("}");
  IElementType REPEAT = new GmlTokenType("repeat");
  IElementType RESOURCES = new GmlTokenType("resources");
  IElementType RETURN = new GmlTokenType("return");
  IElementType ROOM_END = new GmlTokenType("room_end");
  IElementType ROOM_START = new GmlTokenType("room_start");
  IElementType RRBR = new GmlTokenType(")");
  IElementType RSBR = new GmlTokenType("]");
  IElementType RSHIFT = new GmlTokenType(">>");
  IElementType SCRIPT = new GmlTokenType("script");
  IElementType SELF = new GmlTokenType("self");
  IElementType SEMICON = new GmlTokenType(";");
  IElementType STEP = new GmlTokenType("step");
  IElementType STEP_BEGIN = new GmlTokenType("step_begin");
  IElementType STEP_END = new GmlTokenType("step_end");
  IElementType STRING = new GmlTokenType("string");
  IElementType SWITCH = new GmlTokenType("switch");
  IElementType TRUE = new GmlTokenType("true");
  IElementType UNTIL = new GmlTokenType("until");
  IElementType USER_EVENT_0 = new GmlTokenType("user_event_0");
  IElementType USER_EVENT_1 = new GmlTokenType("user_event_1");
  IElementType USER_EVENT_10 = new GmlTokenType("user_event_10");
  IElementType USER_EVENT_11 = new GmlTokenType("user_event_11");
  IElementType USER_EVENT_12 = new GmlTokenType("user_event_12");
  IElementType USER_EVENT_13 = new GmlTokenType("user_event_13");
  IElementType USER_EVENT_14 = new GmlTokenType("user_event_14");
  IElementType USER_EVENT_15 = new GmlTokenType("user_event_15");
  IElementType USER_EVENT_2 = new GmlTokenType("user_event_2");
  IElementType USER_EVENT_3 = new GmlTokenType("user_event_3");
  IElementType USER_EVENT_4 = new GmlTokenType("user_event_4");
  IElementType USER_EVENT_5 = new GmlTokenType("user_event_5");
  IElementType USER_EVENT_6 = new GmlTokenType("user_event_6");
  IElementType USER_EVENT_7 = new GmlTokenType("user_event_7");
  IElementType USER_EVENT_8 = new GmlTokenType("user_event_8");
  IElementType USER_EVENT_9 = new GmlTokenType("user_event_9");
  IElementType VAR = new GmlTokenType("var");
  IElementType WHILE = new GmlTokenType("while");
  IElementType WINDOW_RESIZE = new GmlTokenType("window_resize");
  IElementType WITH = new GmlTokenType("with");
  IElementType XOR = new GmlTokenType("^");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == AFTER_INDENTIFIER) {
        return new GmlAfterIndentifierImpl(node);
      }
      else if (type == ASSIGN_EXPRESSION) {
        return new GmlAssignExpressionImpl(node);
      }
      else if (type == ASSIGN_OP) {
        return new GmlAssignOpImpl(node);
      }
      else if (type == CASE_STATEMENT) {
        return new GmlCaseStatementImpl(node);
      }
      else if (type == COLLISION_EVENT) {
        return new GmlCollisionEventImpl(node);
      }
      else if (type == COMPUTE_EXPRESSION) {
        return new GmlComputeExpressionImpl(node);
      }
      else if (type == CONDITIONAL_EXPRESSION) {
        return new GmlConditionalExpressionImpl(node);
      }
      else if (type == CONLESS_STATEMENT) {
        return new GmlConlessStatementImpl(node);
      }
      else if (type == CONST_DECLARATION) {
        return new GmlConstDeclarationImpl(node);
      }
      else if (type == DEFAULT_STATEMENT) {
        return new GmlDefaultStatementImpl(node);
      }
      else if (type == DEF_COSNT) {
        return new GmlDefCosntImpl(node);
      }
      else if (type == DO_STATEMENT) {
        return new GmlDoStatementImpl(node);
      }
      else if (type == ENUM_DECLARATION) {
        return new GmlEnumDeclarationImpl(node);
      }
      else if (type == ENUM_ENTRY_DECLARATION) {
        return new GmlEnumEntryDeclarationImpl(node);
      }
      else if (type == EVENT_DECLARATION) {
        return new GmlEventDeclarationImpl(node);
      }
      else if (type == EVENT_TYPE) {
        return new GmlEventTypeImpl(node);
      }
      else if (type == EXPR_OP) {
        return new GmlExprOpImpl(node);
      }
      else if (type == FOR_STATEMENT) {
        return new GmlForStatementImpl(node);
      }
      else if (type == FUNCTION_ARG_LIST) {
        return new GmlFunctionArgListImpl(node);
      }
      else if (type == FUNCTION_ARG_LIST_DECL) {
        return new GmlFunctionArgListDeclImpl(node);
      }
      else if (type == FUNCTION_DECLARATION) {
        return new GmlFunctionDeclarationImpl(node);
      }
      else if (type == FUNCTION_DECLARATION_NAME) {
        return new GmlFunctionDeclarationNameImpl(node);
      }
      else if (type == FUNC_ARG_DECL) {
        return new GmlFuncArgDeclImpl(node);
      }
      else if (type == FUNC_RETURNVAL) {
        return new GmlFuncReturnvalImpl(node);
      }
      else if (type == FUNC_RETURN_NAME) {
        return new GmlFuncReturnNameImpl(node);
      }
      else if (type == GLOBALVAR_DECLARATION) {
        return new GmlGlobalvarDeclarationImpl(node);
      }
      else if (type == GML_BUILTIN_CONSTANT) {
        return new GmlGmlBuiltinConstantImpl(node);
      }
      else if (type == GML_BUILTIN_CONSTANTS) {
        return new GmlGmlBuiltinConstantsImpl(node);
      }
      else if (type == GML_FUNCTIONS) {
        return new GmlGmlFunctionsImpl(node);
      }
      else if (type == GML_INSTANCE_VARIABLE) {
        return new GmlGmlInstanceVariableImpl(node);
      }
      else if (type == GML_INSTANCE_VARIABLES) {
        return new GmlGmlInstanceVariablesImpl(node);
      }
      else if (type == GML_OBJECT) {
        return new GmlGmlObjectImpl(node);
      }
      else if (type == GML_RESOURCE) {
        return new GmlGmlResourceImpl(node);
      }
      else if (type == GML_RESOURCES) {
        return new GmlGmlResourcesImpl(node);
      }
      else if (type == GML_SCRIPT) {
        return new GmlGmlScriptImpl(node);
      }
      else if (type == IDENTIFIER_PLUS) {
        return new GmlIdentifierPlusImpl(node);
      }
      else if (type == IDENTIFIER_REF) {
        return new GmlIdentifierRefImpl(node);
      }
      else if (type == IF_STATEMENT) {
        return new GmlIfStatementImpl(node);
      }
      else if (type == INLINE_FUNCTION_DECLARATION) {
        return new GmlInlineFunctionDeclarationImpl(node);
      }
      else if (type == LITERAL_EXPRESSION) {
        return new GmlLiteralExpressionImpl(node);
      }
      else if (type == PLUS_MINUS_EXPRESSION) {
        return new GmlPlusMinusExpressionImpl(node);
      }
      else if (type == PLUS_MINUS_OP) {
        return new GmlPlusMinusOpImpl(node);
      }
      else if (type == PLUS_MINUS_REV_EXPRESSION) {
        return new GmlPlusMinusRevExpressionImpl(node);
      }
      else if (type == REFERENCE_EXPRESSION) {
        return new GmlReferenceExpressionImpl(node);
      }
      else if (type == REPEAT_STATEMENT) {
        return new GmlRepeatStatementImpl(node);
      }
      else if (type == RETURN_STATEMENT) {
        return new GmlReturnStatementImpl(node);
      }
      else if (type == ROUND_BRACKET_EXPRESSION) {
        return new GmlRoundBracketExpressionImpl(node);
      }
      else if (type == SCRIPT_CALL) {
        return new GmlScriptCallImpl(node);
      }
      else if (type == SCRIPT_FUNCTION_DECLARATION) {
        return new GmlScriptFunctionDeclarationImpl(node);
      }
      else if (type == SCRIPT_REF) {
        return new GmlScriptRefImpl(node);
      }
      else if (type == STATEMENT) {
        return new GmlStatementImpl(node);
      }
      else if (type == STATEMENT_BLOCK) {
        return new GmlStatementBlockImpl(node);
      }
      else if (type == STRUCT_DECLARATION) {
        return new GmlStructDeclarationImpl(node);
      }
      else if (type == STRUCT_FIELD_DECLARATION) {
        return new GmlStructFieldDeclarationImpl(node);
      }
      else if (type == SWITCH_STATEMENT) {
        return new GmlSwitchStatementImpl(node);
      }
      else if (type == TEST_EXPRESSION) {
        return new GmlTestExpressionImpl(node);
      }
      else if (type == TEST_OP) {
        return new GmlTestOpImpl(node);
      }
      else if (type == VAR_DECLARATION) {
        return new GmlVarDeclarationImpl(node);
      }
      else if (type == WHILE_STATEMENT) {
        return new GmlWhileStatementImpl(node);
      }
      else if (type == WITH_STATEMENT) {
        return new GmlWithStatementImpl(node);
      }
      else if (type == ZERO_ELEVEN) {
        return new GmlZeroElevenImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
