// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GmlVarDeclaration extends PsiElement {

  @Nullable
  GmlAfterIndentifier getAfterIndentifier();

  @NotNull
  GmlAssignOp getAssignOp();

  @Nullable
  GmlExpression getExpression();

  @Nullable
  GmlInlineFunctionDeclaration getInlineFunctionDeclaration();

  @NotNull
  PsiElement getIdentifier();

  boolean isLocal();

  String getName();

}
