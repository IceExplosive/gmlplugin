// This is a generated file. Not intended for manual editing.
package gmlplugin.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class GmlVisitor extends PsiElementVisitor {

  public void visitAfterIndentifier(@NotNull GmlAfterIndentifier o) {
    visitPsiElement(o);
  }

  public void visitAssignExpression(@NotNull GmlAssignExpression o) {
    visitExpression(o);
  }

  public void visitAssignOp(@NotNull GmlAssignOp o) {
    visitPsiElement(o);
  }

  public void visitCaseStatement(@NotNull GmlCaseStatement o) {
    visitPsiElement(o);
  }

  public void visitCollisionEvent(@NotNull GmlCollisionEvent o) {
    visitPsiElement(o);
  }

  public void visitComputeExpression(@NotNull GmlComputeExpression o) {
    visitExpression(o);
  }

  public void visitConditionalExpression(@NotNull GmlConditionalExpression o) {
    visitExpression(o);
  }

  public void visitConlessStatement(@NotNull GmlConlessStatement o) {
    visitPsiElement(o);
  }

  public void visitConstDeclaration(@NotNull GmlConstDeclaration o) {
    visitPsiElement(o);
  }

  public void visitDefCosnt(@NotNull GmlDefCosnt o) {
    visitPsiElement(o);
  }

  public void visitDefaultStatement(@NotNull GmlDefaultStatement o) {
    visitPsiElement(o);
  }

  public void visitDoStatement(@NotNull GmlDoStatement o) {
    visitPsiElement(o);
  }

  public void visitEnumDeclaration(@NotNull GmlEnumDeclaration o) {
    visitPsiElement(o);
  }

  public void visitEnumEntryDeclaration(@NotNull GmlEnumEntryDeclaration o) {
    visitPsiElement(o);
  }

  public void visitEventDeclaration(@NotNull GmlEventDeclaration o) {
    visitPsiElement(o);
  }

  public void visitEventType(@NotNull GmlEventType o) {
    visitPsiElement(o);
  }

  public void visitExprOp(@NotNull GmlExprOp o) {
    visitPsiElement(o);
  }

  public void visitExpression(@NotNull GmlExpression o) {
    visitPsiElement(o);
  }

  public void visitForStatement(@NotNull GmlForStatement o) {
    visitPsiElement(o);
  }

  public void visitFuncArgDecl(@NotNull GmlFuncArgDecl o) {
    visitPsiElement(o);
  }

  public void visitFuncReturnName(@NotNull GmlFuncReturnName o) {
    visitPsiElement(o);
  }

  public void visitFuncReturnval(@NotNull GmlFuncReturnval o) {
    visitPsiElement(o);
  }

  public void visitFunctionArgList(@NotNull GmlFunctionArgList o) {
    visitPsiElement(o);
  }

  public void visitFunctionArgListDecl(@NotNull GmlFunctionArgListDecl o) {
    visitPsiElement(o);
  }

  public void visitFunctionDeclaration(@NotNull GmlFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitFunctionDeclarationName(@NotNull GmlFunctionDeclarationName o) {
    visitPsiElement(o);
  }

  public void visitGlobalvarDeclaration(@NotNull GmlGlobalvarDeclaration o) {
    visitPsiElement(o);
  }

  public void visitGmlBuiltinConstant(@NotNull GmlGmlBuiltinConstant o) {
    visitPsiElement(o);
  }

  public void visitGmlBuiltinConstants(@NotNull GmlGmlBuiltinConstants o) {
    visitPsiElement(o);
  }

  public void visitGmlFunctions(@NotNull GmlGmlFunctions o) {
    visitPsiElement(o);
  }

  public void visitGmlInstanceVariable(@NotNull GmlGmlInstanceVariable o) {
    visitPsiElement(o);
  }

  public void visitGmlInstanceVariables(@NotNull GmlGmlInstanceVariables o) {
    visitPsiElement(o);
  }

  public void visitGmlObject(@NotNull GmlGmlObject o) {
    visitPsiElement(o);
  }

  public void visitGmlResource(@NotNull GmlGmlResource o) {
    visitPsiElement(o);
  }

  public void visitGmlResources(@NotNull GmlGmlResources o) {
    visitPsiElement(o);
  }

  public void visitGmlScript(@NotNull GmlGmlScript o) {
    visitPsiElement(o);
  }

  public void visitIdentifierRef(@NotNull GmlIdentifierRef o) {
    visitPsiElement(o);
  }

  public void visitIdentifierPlus(@NotNull GmlIdentifierPlus o) {
    visitPsiElement(o);
  }

  public void visitIfStatement(@NotNull GmlIfStatement o) {
    visitPsiElement(o);
  }

  public void visitInlineFunctionDeclaration(@NotNull GmlInlineFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitLiteralExpression(@NotNull GmlLiteralExpression o) {
    visitExpression(o);
  }

  public void visitPlusMinusExpression(@NotNull GmlPlusMinusExpression o) {
    visitExpression(o);
  }

  public void visitPlusMinusOp(@NotNull GmlPlusMinusOp o) {
    visitPsiElement(o);
  }

  public void visitPlusMinusRevExpression(@NotNull GmlPlusMinusRevExpression o) {
    visitExpression(o);
  }

  public void visitReferenceExpression(@NotNull GmlReferenceExpression o) {
    visitExpression(o);
  }

  public void visitRepeatStatement(@NotNull GmlRepeatStatement o) {
    visitPsiElement(o);
  }

  public void visitReturnStatement(@NotNull GmlReturnStatement o) {
    visitPsiElement(o);
  }

  public void visitRoundBracketExpression(@NotNull GmlRoundBracketExpression o) {
    visitExpression(o);
  }

  public void visitScriptCall(@NotNull GmlScriptCall o) {
    visitPsiElement(o);
  }

  public void visitScriptFunctionDeclaration(@NotNull GmlScriptFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitScriptRef(@NotNull GmlScriptRef o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull GmlStatement o) {
    visitPsiElement(o);
  }

  public void visitStatementBlock(@NotNull GmlStatementBlock o) {
    visitPsiElement(o);
  }

  public void visitStructDeclaration(@NotNull GmlStructDeclaration o) {
    visitExpression(o);
  }

  public void visitStructFieldDeclaration(@NotNull GmlStructFieldDeclaration o) {
    visitPsiElement(o);
  }

  public void visitSwitchStatement(@NotNull GmlSwitchStatement o) {
    visitPsiElement(o);
  }

  public void visitTestExpression(@NotNull GmlTestExpression o) {
    visitExpression(o);
  }

  public void visitTestOp(@NotNull GmlTestOp o) {
    visitPsiElement(o);
  }

  public void visitVarDeclaration(@NotNull GmlVarDeclaration o) {
    visitPsiElement(o);
  }

  public void visitWhileStatement(@NotNull GmlWhileStatement o) {
    visitPsiElement(o);
  }

  public void visitWithStatement(@NotNull GmlWithStatement o) {
    visitPsiElement(o);
  }

  public void visitZeroEleven(@NotNull GmlZeroEleven o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
