// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import gmlplugin.psi.*;

public class GmlComputeExpressionImpl extends GmlExpressionImpl implements GmlComputeExpression {

  public GmlComputeExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitComputeExpression(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public GmlExprOp getExprOp() {
    return findNotNullChildByClass(GmlExprOp.class);
  }

  @Override
  @NotNull
  public List<GmlExpression> getExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, GmlExpression.class);
  }

}
