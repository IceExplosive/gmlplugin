// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import gmlplugin.psi.*;

public class GmlConlessStatementImpl extends ASTWrapperPsiElement implements GmlConlessStatement {

  public GmlConlessStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitConlessStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public GmlConstDeclaration getConstDeclaration() {
    return findChildByClass(GmlConstDeclaration.class);
  }

  @Override
  @Nullable
  public GmlDoStatement getDoStatement() {
    return findChildByClass(GmlDoStatement.class);
  }

  @Override
  @Nullable
  public GmlEnumDeclaration getEnumDeclaration() {
    return findChildByClass(GmlEnumDeclaration.class);
  }

  @Override
  @Nullable
  public GmlExpression getExpression() {
    return findChildByClass(GmlExpression.class);
  }

  @Override
  @Nullable
  public GmlForStatement getForStatement() {
    return findChildByClass(GmlForStatement.class);
  }

  @Override
  @Nullable
  public GmlGlobalvarDeclaration getGlobalvarDeclaration() {
    return findChildByClass(GmlGlobalvarDeclaration.class);
  }

  @Override
  @Nullable
  public GmlIfStatement getIfStatement() {
    return findChildByClass(GmlIfStatement.class);
  }

  @Override
  @Nullable
  public GmlRepeatStatement getRepeatStatement() {
    return findChildByClass(GmlRepeatStatement.class);
  }

  @Override
  @Nullable
  public GmlReturnStatement getReturnStatement() {
    return findChildByClass(GmlReturnStatement.class);
  }

  @Override
  @Nullable
  public GmlStatementBlock getStatementBlock() {
    return findChildByClass(GmlStatementBlock.class);
  }

  @Override
  @Nullable
  public GmlSwitchStatement getSwitchStatement() {
    return findChildByClass(GmlSwitchStatement.class);
  }

  @Override
  @Nullable
  public GmlVarDeclaration getVarDeclaration() {
    return findChildByClass(GmlVarDeclaration.class);
  }

  @Override
  @Nullable
  public GmlWhileStatement getWhileStatement() {
    return findChildByClass(GmlWhileStatement.class);
  }

  @Override
  @Nullable
  public GmlWithStatement getWithStatement() {
    return findChildByClass(GmlWithStatement.class);
  }

  @Override
  @Nullable
  public PsiElement getComment() {
    return findChildByType(COMMENT);
  }

}
