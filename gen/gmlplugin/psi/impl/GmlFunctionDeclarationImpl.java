// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import gmlplugin.psi.*;

public class GmlFunctionDeclarationImpl extends ASTWrapperPsiElement implements GmlFunctionDeclaration {

  public GmlFunctionDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitFunctionDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public GmlFuncReturnval getFuncReturnval() {
    return findNotNullChildByClass(GmlFuncReturnval.class);
  }

  @Override
  @NotNull
  public GmlFunctionArgListDecl getFunctionArgListDecl() {
    return findNotNullChildByClass(GmlFunctionArgListDecl.class);
  }

  @Override
  @NotNull
  public GmlFunctionDeclarationName getFunctionDeclarationName() {
    return findNotNullChildByClass(GmlFunctionDeclarationName.class);
  }

}
