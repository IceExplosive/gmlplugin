// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import gmlplugin.psi.*;

public class GmlIdentifierPlusImpl extends ASTWrapperPsiElement implements GmlIdentifierPlus {

  public GmlIdentifierPlusImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitIdentifierPlus(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<GmlAssignOp> getAssignOpList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, GmlAssignOp.class);
  }

  @Override
  @NotNull
  public List<GmlEventType> getEventTypeList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, GmlEventType.class);
  }

  @Override
  @NotNull
  public List<GmlExprOp> getExprOpList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, GmlExprOp.class);
  }

}
