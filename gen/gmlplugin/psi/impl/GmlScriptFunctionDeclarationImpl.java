// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import gmlplugin.psi.*;
import com.intellij.navigation.ItemPresentation;

public class GmlScriptFunctionDeclarationImpl extends ASTWrapperPsiElement implements GmlScriptFunctionDeclaration {

  public GmlScriptFunctionDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitScriptFunctionDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public GmlFunctionArgList getFunctionArgList() {
    return findChildByClass(GmlFunctionArgList.class);
  }

  @Override
  @NotNull
  public GmlStatementBlock getStatementBlock() {
    return findNotNullChildByClass(GmlStatementBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

  @Override
  public ItemPresentation getPresentation() {
    return GmlPsiImplUtils.getPresentation(this);
  }

  @Override
  public String getName() {
    return GmlPsiImplUtils.getName(this);
  }

}
