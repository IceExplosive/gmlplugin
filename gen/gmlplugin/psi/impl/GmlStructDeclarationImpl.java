// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import gmlplugin.psi.*;

public class GmlStructDeclarationImpl extends GmlExpressionImpl implements GmlStructDeclaration {

  public GmlStructDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitStructDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<GmlStructFieldDeclaration> getStructFieldDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, GmlStructFieldDeclaration.class);
  }

}
