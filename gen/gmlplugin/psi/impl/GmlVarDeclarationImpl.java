// This is a generated file. Not intended for manual editing.
package gmlplugin.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static gmlplugin.psi.GmlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import gmlplugin.psi.*;

public class GmlVarDeclarationImpl extends ASTWrapperPsiElement implements GmlVarDeclaration {

  public GmlVarDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull GmlVisitor visitor) {
    visitor.visitVarDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof GmlVisitor) accept((GmlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public GmlAfterIndentifier getAfterIndentifier() {
    return findChildByClass(GmlAfterIndentifier.class);
  }

  @Override
  @NotNull
  public GmlAssignOp getAssignOp() {
    return findNotNullChildByClass(GmlAssignOp.class);
  }

  @Override
  @Nullable
  public GmlExpression getExpression() {
    return findChildByClass(GmlExpression.class);
  }

  @Override
  @Nullable
  public GmlInlineFunctionDeclaration getInlineFunctionDeclaration() {
    return findChildByClass(GmlInlineFunctionDeclaration.class);
  }

  @Override
  @NotNull
  public PsiElement getIdentifier() {
    return findNotNullChildByType(IDENTIFIER);
  }

  @Override
  public boolean isLocal() {
    return GmlPsiImplUtils.isLocal(this);
  }

  @Override
  public String getName() {
    return GmlPsiImplUtils.getName(this);
  }

}
