package gmlplugin.Files;

import com.intellij.openapi.fileTypes.LanguageFileType;
import gmlplugin.GmLanguage;
import gmlplugin.icons.GmlIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class GmlFileType extends LanguageFileType {
    public static final GmlFileType INSTANCE = new GmlFileType();

    private GmlFileType() {
        super(GmLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Gm object";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Game maker's object";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "gmo";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return GmlIcons.FILE;
    }

}
