package gmlplugin.Files;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import gmlplugin.GmlFile;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class GmlFileUtils {

    public static Collection<VirtualFile> getAllFiles(final Project project) {
        return FileTypeIndex.getFiles(GmlFileType.INSTANCE, GlobalSearchScope.allScope(project));
    }

    public static Collection<GmlFile> getAllGmlFiles(final Project project) {
        Collection<GmlFile> files = new ArrayList<>();
        for (VirtualFile file : getAllFiles(project)) {
            files.add((GmlFile) PsiManager.getInstance(project).findFile(file));
        }

        return files;
    }

    public static GmlFile findGmlFile(@NotNull final Project project, @NotNull final VirtualFile virtualFile) {
        return (GmlFile) PsiManager.getInstance(project).findFile(virtualFile);
    }

}
