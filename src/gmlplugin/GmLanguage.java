package gmlplugin;

import com.intellij.lang.Language;

public class GmLanguage extends Language {
    public static final GmLanguage INSTANCE = new GmLanguage();

    private GmLanguage() {
        super("GameMaker");
    }
}
