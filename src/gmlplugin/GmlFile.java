package gmlplugin;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import gmlplugin.Files.GmlFileType;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class GmlFile extends PsiFileBase {
    public GmlFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, GmLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return GmlFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Gml File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}