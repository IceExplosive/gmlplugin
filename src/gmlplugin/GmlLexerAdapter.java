package gmlplugin;

import com.intellij.lexer.FlexAdapter;
import java.io.Reader;

public class GmlLexerAdapter  extends FlexAdapter {
    public GmlLexerAdapter() {
        super(new GmlLexer((Reader) null));
    }
}
