package gmlplugin.codeFormatting;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.tree.IElementType;
import gmlplugin.psi.GmlTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class GmlBlock extends AbstractBlock {
    private static final ArrayList<IElementType> indentList = new ArrayList<>();

    static {
        indentList.add(GmlTypes.EVENT_DECLARATION);
        indentList.add(GmlTypes.STATEMENT_BLOCK);
        indentList.add(GmlTypes.STRUCT_DECLARATION);
    }

    private final SpacingBuilder spacingBuilder;
    private Indent indent;

    public GmlBlock(@NotNull ASTNode node, @Nullable Wrap wrap, @Nullable Alignment alignment, SpacingBuilder spacingBuilder, Indent indent) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
        this.indent = indent;
    }

    @Override
    public Indent getIndent() {
        return indent;
    }

    @Nullable
    @Override
    public Spacing getSpacing(@Nullable Block block, @NotNull Block block1) {
        return spacingBuilder.getSpacing(this, block, block1);
    }

    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }

    @NotNull
    @Override
    public ChildAttributes getChildAttributes(int newChildIndex) {
        return new ChildAttributes(getIndent(getNode()), null);
    }

    @Override
    protected List<Block> buildChildren() {
        List<Block> blocks = new ArrayList<>();

        for (ASTNode node : getNode().getChildren(null)) {
            if (isNodeEmpty(node)) {
                continue;
            }

            blocks.add(new GmlBlock(
                    node,
                    Wrap.createWrap(WrapType.NONE, false),
                    null, //TODO Alignment.createAlignment(),
                    spacingBuilder,
                    getIndent(getNode().getTreeParent())
            ));
        }

        return blocks;
    }

    private Indent getIndent(@Nullable final ASTNode parentNode) {
        if (parentNode != null && indentList.contains(parentNode.getElementType())) {
            return Indent.getNormalIndent(false);
        }

        return Indent.getNoneIndent();
    }

    private boolean isNodeEmpty(@Nullable ASTNode node) {
        return node == null
                || node.getElementType() == TokenType.WHITE_SPACE
                || node.getTextLength() == 0;
    }
}
