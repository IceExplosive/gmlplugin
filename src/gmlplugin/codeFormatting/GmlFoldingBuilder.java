package gmlplugin.codeFormatting;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingBuilderEx;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.FoldingGroup;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.util.PsiTreeUtil;
import gmlplugin.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GmlFoldingBuilder extends FoldingBuilderEx {
    @NotNull
    @Override
    public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement psiElement, @NotNull Document document, boolean b) {
        List<FoldingDescriptor> descs = new ArrayList<>();

        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlEventDeclaration.class), descs, GmlTypes.LCBR);
        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlForStatement.class), descs, GmlTypes.STATEMENT);
        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlIfStatement.class), descs, GmlTypes.STATEMENT);
        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlWhileStatement.class), descs, GmlTypes.STATEMENT);
        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlRepeatStatement.class), descs, GmlTypes.STATEMENT);
        processDoElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlDoStatement.class), descs);
        processElement(PsiTreeUtil.findChildrenOfType(psiElement, GmlSwitchStatement.class), descs, GmlTypes.LCBR);

        return descs.toArray(new FoldingDescriptor[0]);
    }

    @Nullable
    @Override
    public String getPlaceholderText(@NotNull ASTNode astNode) {
        return "...";
    }

    @Override
    public boolean isCollapsedByDefault(@NotNull ASTNode astNode) {
        return false;
    }

    private void processElement(Collection<PsiElement> coll, List<FoldingDescriptor> descs, IElementType lookFor) {
        for (PsiElement stm : coll) {
            ASTNode eventNode = stm.getNode();
            ASTNode start = eventNode.findChildByType(lookFor);
            TextRange range = eventNode.getTextRange();

            descs.add(new FoldingDescriptor(eventNode, new TextRange(
                    start != null ? start.getStartOffset() : eventNode.getStartOffset(),
                    range.getEndOffset()
            ), FoldingGroup.newGroup("gml")) {
                public String getPlaceholderText() {
                    return "{ ... }";
                }
            });
        }
    }

    private void processDoElement(Collection<PsiElement> coll, List<FoldingDescriptor> descs) {
        for (PsiElement stm : coll) {
            ASTNode eventNode = stm.getNode();
            ASTNode start = eventNode.findChildByType(GmlTypes.LCBR);
            ASTNode end = eventNode.findChildByType(GmlTypes.UNTIL);

            descs.add(new FoldingDescriptor(eventNode, new TextRange(
                    start != null ? start.getStartOffset() : eventNode.getStartOffset(),
                    end.getTextRange().getStartOffset() - 1
            ), FoldingGroup.newGroup("gml")) {
                public String getPlaceholderText() {
                    return "{ ... }";
                }
            });
        }
    }
}
