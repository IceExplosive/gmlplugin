package gmlplugin.codeFormatting;

import com.intellij.formatting.*;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import gmlplugin.GmLanguage;
import gmlplugin.psi.GmlTypes;
import org.codehaus.groovy.ast.ASTNode;
import org.jetbrains.annotations.NotNull;

public class GmlFormattingModelBuilder implements FormattingModelBuilder {
    private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
        return new SpacingBuilder(settings, GmLanguage.INSTANCE);
//                .around(GmlTypes.EVENT_TYPE)
//                .spaceIf(settings.getCommonSettings(GmLanguage.INSTANCE.getID()).SPACE_AROUND_ASSIGNMENT_OPERATORS)
//                .before(GmlTypes.PROPERTY)
//                .spaces(1);
    }

    @NotNull
    @Override
    public FormattingModel createModel(PsiElement element, CodeStyleSettings settings) {
        return FormattingModelProvider
                .createFormattingModelForPsiFile(element.getContainingFile(),
                        new GmlBlock(element.getNode(),
                                Wrap.createWrap(WrapType.NONE, false),
                                Alignment.createAlignment(),
                                createSpaceBuilder(settings),
                                Indent.getNoneIndent()
                        ),
                        settings);
    }
}
