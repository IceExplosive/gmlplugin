package gmlplugin.highlights;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import gmlplugin.icons.GmlIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class GmlColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Keys", GmlHighlighterColors.KEYWORD),
            new AttributesDescriptor("Strings", GmlHighlighterColors.STRING),
            new AttributesDescriptor("Numbers", GmlHighlighterColors.NUMBER),
            new AttributesDescriptor("Variables", GmlHighlighterColors.IDENTIFIER),
            new AttributesDescriptor("Object built-in variables", GmlHighlighterColors.OBJECT_VARIABLES),
            new AttributesDescriptor("Comments", GmlHighlighterColors.COMMENT),
            new AttributesDescriptor("Globals", GmlHighlighterColors.GLOBAL_VARIABLES),
            new AttributesDescriptor("Scripts", GmlHighlighterColors.SCRIPTS),
            new AttributesDescriptor("Resources", GmlHighlighterColors.RESOURCES),
            new AttributesDescriptor("Enum", GmlHighlighterColors.ENUM),
            new AttributesDescriptor("EnumEntry", GmlHighlighterColors.ENUM_ENTRY),
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return GmlIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new GmlSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "#macro sd 44.644\n" +
                "globalvar g_var;\n" +
                "enum Enum {\n" +
                "\tvalue\n" +
                "}\n" +
                "event create {\n" +
                "\tdraw_text(0, 0, \"text\");\n" +
                "\n" +
                "}\n" +
                "event update {\n" +
                "\n" +
                "}";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "GameMaker";
    }
}
