package gmlplugin.highlights;

import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;

public interface GmlHighlighterColors {

    TextAttributesKey KEYWORD = TextAttributesKey.createTextAttributesKey(
                    "GML_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD
    );

    TextAttributesKey NUMBER = TextAttributesKey.createTextAttributesKey(
            "GML_NUMBER", DefaultLanguageHighlighterColors.NUMBER
    );

    TextAttributesKey IDENTIFIER = TextAttributesKey.createTextAttributesKey(
            "GML_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey COMMENT = TextAttributesKey.createTextAttributesKey(
            "GML_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT
    );

    TextAttributesKey STRING = TextAttributesKey.createTextAttributesKey(
            "GML_STRING", DefaultLanguageHighlighterColors.STRING
    );

    TextAttributesKey BAD_CHARACTER = TextAttributesKey.createTextAttributesKey(
            "GML_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER
    );

    TextAttributesKey LOCAL_IDENTIFIER = TextAttributesKey.createTextAttributesKey(
            "GML_LOCAL_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey OBJECT_VARIABLES = TextAttributesKey.createTextAttributesKey(
            "GML_OBJECT_VARIABLES", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey GLOBAL_VARIABLES = TextAttributesKey.createTextAttributesKey(
            "GML_GLOBAL", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey SCRIPTS = TextAttributesKey.createTextAttributesKey(
            "GML_SCRIPTS", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey RESOURCES = TextAttributesKey.createTextAttributesKey(
            "GML_RESOURCES", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey ENUM = TextAttributesKey.createTextAttributesKey(
            "GML_ENUM", DefaultLanguageHighlighterColors.IDENTIFIER
    );

    TextAttributesKey ENUM_ENTRY = TextAttributesKey.createTextAttributesKey(
            "GML_ENUM_ENTRY", DefaultLanguageHighlighterColors.IDENTIFIER
    );

}
