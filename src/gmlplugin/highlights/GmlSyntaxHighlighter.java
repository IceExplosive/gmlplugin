package gmlplugin.highlights;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import gmlplugin.Files.GmlTokenTypeSet;
import gmlplugin.GmlLexerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class GmlSyntaxHighlighter extends SyntaxHighlighterBase {
    private static Map<IElementType, TextAttributesKey> ATTRIBUTES = new HashMap<>();

    static {
        fillMap(ATTRIBUTES, GmlTokenTypeSet.KEYWORDS, GmlHighlighterColors.KEYWORD);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.EVENT_TYPES, GmlHighlighterColors.KEYWORD);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.IDENTIFIERS, GmlHighlighterColors.IDENTIFIER);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.NUMBERS, GmlHighlighterColors.NUMBER);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.DEF_CONSTS, GmlHighlighterColors.NUMBER);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.STRINGS, GmlHighlighterColors.STRING);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.BAD_CHARACTERS, GmlHighlighterColors.BAD_CHARACTER);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.OBJECT_VARIABLES, GmlHighlighterColors.OBJECT_VARIABLES);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.COMMENT, GmlHighlighterColors.COMMENT);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.GLOBAL_VARIABLES, GmlHighlighterColors.GLOBAL_VARIABLES);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.SCRIPTS, GmlHighlighterColors.SCRIPTS);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.RESOURCES, GmlHighlighterColors.RESOURCES);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.ENUM, GmlHighlighterColors.ENUM);
        fillMap(ATTRIBUTES, GmlTokenTypeSet.ENUM_ENTRY, GmlHighlighterColors.ENUM_ENTRY);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new GmlLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType iElementType) {
        return pack(ATTRIBUTES.get(iElementType));
    }
}
