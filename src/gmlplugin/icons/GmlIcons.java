package gmlplugin.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class GmlIcons {
    public static final Icon FILE = IconLoader.getIcon("./icon_tileset_c.png");
    public static final Icon OBJECT = IconLoader.getIcon("./icon_object_c.png");
    public static final Icon SCRIPT = IconLoader.getIcon("./icon_RT_script_c.png");
    public static final Icon LOCAL = IconLoader.getIcon("./icon_bolt_c.png");
    public static final Icon CONST = IconLoader.getIcon("./icon_world-wide_c.png");
    public static final Icon VARIABLE = IconLoader.getIcon("./icon_levels_c.png");
    public static final Icon OBJECT_VAR = IconLoader.getIcon("./icon_sitemap_c.png");
    public static final Icon GLOBAL_VAR = IconLoader.getIcon("./icon_world-wide_c2.png");
}
