package gmlplugin.psi;

import com.intellij.lang.ASTNode;
import com.intellij.lang.annotation.*;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.tree.LeafPsiElement;
import com.intellij.psi.impl.source.tree.java.PsiIdentifierImpl;
import gmlplugin.highlights.GmlHighlighterColors;
//import gmlplugin.psi.GmlVarDeclaration;
import org.intellij.lang.annotations.Identifier;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GmlAnnotator implements Annotator {
    @Override
    public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {
        ASTNode nod = element.getNode();
        if (element instanceof GmlIdentifierRef || nod.getElementType().toString().equals("identifier")) {
            if (element.getText().equals("other")) return;

            String fileName = element.getContainingFile().getName();
            Project project = element.getProject();
            int res = GmlUtils.variableExists(project, element.getText(), fileName, element);

            TextRange range = element.getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            switch (res) {
                case 0: // variables might be defined in scripts for example...
                    break;
                case 1:
                    annot.setTextAttributes(GmlHighlighterColors.LOCAL_IDENTIFIER);
                    break;
                case 2: // Numbers + builtin constants
                    annot.setTextAttributes(GmlHighlighterColors.NUMBER);
                    break;
                case 3:
                    annot.setTextAttributes(GmlHighlighterColors.GLOBAL_VARIABLES);
                    break;
                case 4: // script names
                    annot.setTextAttributes(GmlHighlighterColors.SCRIPTS);
                    break;
                case 5: // objects, shaders, sprites, fonts
                    annot.setTextAttributes(GmlHighlighterColors.RESOURCES);
                    break;
                case 6:
                    annot.setTextAttributes(GmlHighlighterColors.ENUM);
                    break;
                case 7:
                    annot.setTextAttributes(GmlHighlighterColors.ENUM_ENTRY);
                    break;
                case 8:
                    annot.setTextAttributes(GmlHighlighterColors.OBJECT_VARIABLES);
                    break;
            }
            //holder.createErrorAnnotation(range, "Undefined variable");
        } else if (element instanceof GmlVarDeclaration) {
            GmlVarDeclaration var = (GmlVarDeclaration) element;
            if (var.isLocal()) {
                TextRange range = var.getIdentifier().getTextRange();
                Annotation annot = holder.createInfoAnnotation(range, null);
                annot.setTextAttributes(GmlHighlighterColors.LOCAL_IDENTIFIER);
            } else if (GmlUtils.isObject(element.getProject(), ((GmlVarDeclaration) element).getName())) {
                // TODO uhledněji...
                Annotation annot = holder.createInfoAnnotation(((GmlVarDeclaration) element).getIdentifier().getTextRange(), null);
                annot.setTextAttributes(GmlHighlighterColors.NUMBER);
            } else if (GmlUtils.isGlobal(element.getProject(), ((GmlVarDeclaration) element).getName())) {
                // TODO uhledněji...
                Annotation annot = holder.createInfoAnnotation(((GmlVarDeclaration) element).getIdentifier().getTextRange(), null);
                annot.setTextAttributes(GmlHighlighterColors.GLOBAL_VARIABLES);
            }
        } else if (element instanceof GmlConstDeclaration) {
            GmlConstDeclaration var = (GmlConstDeclaration) element;
            TextRange range = var.getIdentifier().getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            annot.setTextAttributes(GmlHighlighterColors.NUMBER);
        } else if (element instanceof GmlGlobalvarDeclaration) {
            GmlGlobalvarDeclaration var = (GmlGlobalvarDeclaration) element;
            TextRange range = var.getIdentifier().getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            annot.setTextAttributes(GmlHighlighterColors.GLOBAL_VARIABLES);
        } else if (element instanceof GmlScriptRef) {
            GmlScriptRef var = (GmlScriptRef) element;
            TextRange range = var.getIdentifier().getTextRange();

            Annotation annot = holder.createInfoAnnotation(range, null);
            switch (GmlUtils.scriptExists(element.getProject(), var.getName())) {
                case 0:
                    annot.setTextAttributes(GmlHighlighterColors.KEYWORD);
                    break;
                case 1:
                    annot.setTextAttributes(GmlHighlighterColors.LOCAL_IDENTIFIER);
                    break;
                default:
                holder.createErrorAnnotation(range, "Undefined script");
            }
        } else if (element instanceof GmlCollisionEvent) {
            GmlCollisionEvent var = (GmlCollisionEvent) element;
            TextRange range = var.getIdentifier().getTextRange();
            if (GmlUtils.objectExists(element.getProject(), var.getIdentifier().getText())) {
                Annotation annot = holder.createInfoAnnotation(range, null);
                annot.setTextAttributes(GmlHighlighterColors.KEYWORD);
            } else {
                holder.createErrorAnnotation(range, "Undefined object");
            }
        } else if (element instanceof GmlScriptFunctionDeclaration) {
            GmlScriptFunctionDeclaration var = (GmlScriptFunctionDeclaration) element;
            TextRange range = var.getIdentifier().getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            annot.setTextAttributes(GmlHighlighterColors.KEYWORD);
        } else if (element instanceof GmlEventDeclaration) {
            GmlEventDeclaration var = (GmlEventDeclaration) element;
            if (!GmlUtils.isUnique(var)) {
                TextRange range = var.getEventType().getTextRange();
                holder.createErrorAnnotation(range, "Event already defined");
            }
        } else if (element instanceof GmlEnumDeclaration) {
            GmlEnumDeclaration var = (GmlEnumDeclaration) element;
            TextRange range = var.getIdentifier().getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            annot.setTextAttributes(GmlHighlighterColors.ENUM);
        } else if (element instanceof GmlEnumEntryDeclaration) {
            GmlEnumEntryDeclaration var = (GmlEnumEntryDeclaration) element;
            TextRange range = var.getIdentifier().getTextRange();
            Annotation annot = holder.createInfoAnnotation(range, null);
            annot.setTextAttributes(GmlHighlighterColors.ENUM_ENTRY);
        }
    }
}
