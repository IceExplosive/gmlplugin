package gmlplugin.psi;

import com.intellij.navigation.ChooseByNameContributor;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import gmlplugin.Files.GmlFileType;
import gmlplugin.GmlFile;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class GmlChooseByNameContributor implements ChooseByNameContributor {
    @NotNull
    @Override
    public String[] getNames(Project project, boolean b) {
        List<String> names = new ArrayList<>();

        for (GmlScriptFunctionDeclaration script: getScripts(project)) {
            names.add(script.getName());
        }

        return names.toArray(new String[0]);
    }

    @NotNull
    @Override
    public NavigationItem[] getItemsByName(String s, String s1, Project project, boolean b) {
        Collection<GmlScriptFunctionDeclaration> scripts = getScripts(project);
        return scripts.toArray(new NavigationItem[scripts.size()]);
    }

    private Collection<GmlScriptFunctionDeclaration> getScripts(Project project) {
        Collection<GmlScriptFunctionDeclaration> scripts = new ArrayList<>();

        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(GmlFileType.INSTANCE, GlobalSearchScope.allScope(project));

        for (VirtualFile virtualFile : virtualFiles) {
            GmlFile file = (GmlFile) PsiManager.getInstance(project).findFile(virtualFile);
            scripts.addAll(PsiTreeUtil.findChildrenOfType(file, GmlScriptFunctionDeclaration.class));
        }

        return scripts;
    }

}
