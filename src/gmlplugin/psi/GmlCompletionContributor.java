package gmlplugin.psi;

import com.intellij.codeInsight.completion.*;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import gmlplugin.psi.completions.CommonBuilder;
import gmlplugin.psi.completions.EventBuilder;
import gmlplugin.psi.completions.ObjectBuilder;
import org.jetbrains.annotations.NotNull;

public class GmlCompletionContributor extends CompletionContributor {

    public void fillCompletionVariants(@NotNull final CompletionParameters parameters, @NotNull CompletionResultSet result) {
        PsiElement prev = PsiTreeUtil.prevVisibleLeaf(parameters.getPosition());
        if (prev == null) {
            EventBuilder.addEvent(result);
        } else {
            if (PlatformPatterns.psiElement(GmlTypes.EVENT).accepts(prev)) {
                EventBuilder.addEventTypes(result);

                result.stopHere();
                return;
            }

            if (PlatformPatterns.psiElement(GmlTypes.COLLISION).accepts(prev)) {
                ObjectBuilder.addObjectNames(result, parameters);

                result.stopHere();
                return;
            }

            PsiElement parent = prev.getParent();
            if (PlatformPatterns.psiElement(GmlTypes.EVENT_DECLARATION).accepts(parent)) {
                if (PlatformPatterns.psiElement(GmlTypes.RCBR).accepts(prev)) {
                    EventBuilder.addEvent(result);

                    result.stopHere();
                    return;
                }
            }
        }

        CommonBuilder.addReferences(result, parameters);
    }
}
