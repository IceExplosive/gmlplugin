package gmlplugin.psi;

import com.intellij.psi.tree.IElementType;
import gmlplugin.GmLanguage;
import org.jetbrains.annotations.NotNull;

public class GmlElementType extends IElementType {

    public GmlElementType(@NotNull String debugName) {
        super(debugName, GmLanguage.INSTANCE);
    }

}
