package gmlplugin.psi;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import gmlplugin.Files.GmlFileType;
import gmlplugin.Files.GmlFileUtils;
import gmlplugin.GmlFile;
import gmlplugin.icons.GmlIcons;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Collection;

public class GmlPsiImplUtils {

    public static String getName(GmlVarDeclaration element) {
        return element.getIdentifier().getText();
    }

    public static String getName(GmlEnumDeclaration element) {
        return element.getIdentifier().getText();
    }

    public static String getName(GmlEventDeclaration element) {
        return element.getEventType().getText();
    }

    public static String getResourceName(GmlGmlResource element) {
        return element.getIdentifier().getText();
    }

    public static String getName(GmlGmlInstanceVariable element) {
        return element.getIdentifier().getText();
    }

    public static String getName(GmlGmlBuiltinConstant element) {
        return element.getIdentifier().getText();
    }

    public static boolean isLocal(GmlVarDeclaration element) {
        ASTNode varNode = element.getNode().getFirstChildNode();
        return varNode.getText().equals("var");
    }

    public static String getConstName(GmlConstDeclaration element) {
        return element.getIdentifier().getText();
    }

    public static String getName(GmlScriptRef element) {
        return element.getIdentifier().getText();
    }

    public static String getGlobalName(GmlGlobalvarDeclaration element) {
        ASTNode varNode = element.getNode().findChildByType(GmlTypes.IDENTIFIER);
        if (varNode != null) {
            return varNode.getText();
        }  else {
            return null;
        }
    }

//    public static String getScriptHint(GmlGmlScriptDeclaration element) {
//        GmlFunctionArgList varNode = (GmlFunctionArgList) element.getNode().findChildByType(GmlTypes.FUNCTION_ARG_LIST).getPsi();
//        if (varNode != null) {
//            return varNode.getText();
//        }
//
//        return null;
//    }

    public static String getName(final GmlScriptFunctionDeclaration element) {
        return element.getIdentifier().getText();
    }

    public static ItemPresentation getPresentation(final GmlScriptFunctionDeclaration element) {
        return new ItemPresentation() {
            @Override
            public String getPresentableText() {
                return element.getName();
            }

            @Override
            public String getLocationString() {
                return element.getContainingFile().getName();
            }

            @Override
            public Icon getIcon(boolean b) {
                return GmlIcons.SCRIPT;
            }
        };
    }

}
