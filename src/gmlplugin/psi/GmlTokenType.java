package gmlplugin.psi;

import com.intellij.psi.tree.IElementType;
import gmlplugin.GmLanguage;
import org.jetbrains.annotations.NotNull;

public class GmlTokenType extends IElementType {

    public GmlTokenType(@NotNull String debugName) {
        super(debugName, GmLanguage.INSTANCE);
    }

}
