package gmlplugin.psi;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.*;
import com.intellij.psi.util.PsiTreeUtil;
import gmlplugin.Files.GmlFileType;
import gmlplugin.Files.GmlFileUtils;
import gmlplugin.GmlFile;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class GmlUtils {

    public static GmlFunctionArgListDecl getFuncArgList(GmlScriptCall call) {
        Project project = call.getProject();

        String name = call.getScriptRef().getName();
        String filename = name + ".gmo";

        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            String fname = file.getName();

            if (fname.equals("_functionList.gmo")) {
                Collection<GmlFunctionDeclaration> list = PsiTreeUtil.collectElementsOfType(file, GmlFunctionDeclaration.class);
                for (GmlFunctionDeclaration decl : list) {
                    if (decl.getFunctionDeclarationName().getText().equals(name)) {
                        return decl.getFunctionArgListDecl();
                    }
                }
            } else if (fname.equals(filename)) {
                return PsiTreeUtil.findChildOfType(file.getFirstChild(), GmlFunctionArgListDecl.class);
            }
        }

        return null;
    }

    public static boolean isGlobal(Project project, String key) {
        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            if (file != null) {
                Collection<GmlGlobalvarDeclaration> properties = PsiTreeUtil.collectElementsOfType(file, GmlGlobalvarDeclaration.class);
                for (GmlGlobalvarDeclaration property : properties) {
                    if (key.equals(property.getGlobalName())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static boolean isObject(Project project, String key) {
        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            PsiElement child = file.getFirstChild();
            while (child instanceof PsiWhiteSpace) {
                child = child.getNextSibling();
            }

            if (child instanceof GmlGmlObject) {
                if (file.getName().equals(key + ".gmo")) {
                    return true;
                }
            }
        }

        return false;
    }

    public static int scriptExists(Project project, String key) {
        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            if (file.getName().equals("_functionList.gmo")) {
                Collection<GmlFunctionDeclarationName> functions = PsiTreeUtil.collectElementsOfType(file, GmlFunctionDeclarationName.class);
                for (GmlFunctionDeclarationName name : functions) {
                    if (name.getText().equals(key)) {
                        return 0;
                    }
                }
            } else {
                Collection<GmlScriptFunctionDeclaration> scripts = PsiTreeUtil.collectElementsOfType(file, GmlScriptFunctionDeclaration.class);
                for (GmlScriptFunctionDeclaration script : scripts) {
                    if (script.getIdentifier().getText().equals(key)) {
                        return 0;
                    }
                }

                Collection<GmlVarDeclaration> variables = PsiTreeUtil.collectElementsOfType(file, GmlVarDeclaration.class);
                for (GmlVarDeclaration variable : variables) {
                    if (variable.getIdentifier().getText().equals(key)) {
                        return 1;
                    }
                }
            }
        }

        return -1;
    }

    public static boolean objectExists(Project project, String key) {
        String filename = key + ".gmo";

        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            if (file != null) {
                PsiElement child = file.getFirstChild();
                while (child instanceof PsiWhiteSpace) {
                    child = child.getNextSibling();
                }

                if (child instanceof GmlGmlObject) {
                    if (file.getName().equals(filename)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static int variableExists(Project project, String key, String fileName, PsiElement el) { // TODO element only
        int result = -1;
        // 0 not found
        // 1 local
        // 2 number, builtin constants
        // 3 global
        // 4 script
        // 5 resource (room, sprite, etc.)
        // 6 enum
        // 7 enum entry
        // 8 instance variable

        for (GmlFile file : GmlFileUtils.getAllGmlFiles(project)) {
            if (file != null) {
                if (file.getName().equals("_resources.gmo")) {
                    for (GmlGmlResource i : PsiTreeUtil.findChildrenOfType(file, GmlGmlResource.class)) {
                        if (i.getText().equals(key)) {
                            return 5;
                        }
                    }
                } else if (file.getName().equals("_instanceVariables.gmo")) {
                    for (GmlGmlInstanceVariable i : PsiTreeUtil.findChildrenOfType(file, GmlGmlInstanceVariable.class)) {
                        if (i.getName().equals(key)) {
                            return 8;
                        }
                    }
                } else if (file.getName().equals("_builtinConstants.gmo")) {
                    for (GmlGmlBuiltinConstant i : PsiTreeUtil.findChildrenOfType(file, GmlGmlBuiltinConstant.class)) {
                        if (i.getName().equals(key)) {
                            return 2;
                        }
                    }
                }

                // TODO zobecnit

                Collection<GmlConstDeclaration> properties2 = PsiTreeUtil.collectElementsOfType(file, GmlConstDeclaration.class);
                for (GmlConstDeclaration property : properties2) {
                    if (key.equals(property.getConstName())) {
                        return 2;
                    }
                }
                Collection<GmlGlobalvarDeclaration> properties3 = PsiTreeUtil.collectElementsOfType(file, GmlGlobalvarDeclaration.class);
                for (GmlGlobalvarDeclaration property : properties3) {
                    if (key.equals(property.getGlobalName())) {
                        return 3;
                    }
                }
                Collection<GmlEnumDeclaration> enums = PsiTreeUtil.collectElementsOfType(file, GmlEnumDeclaration.class);
                for (GmlEnumDeclaration enumName : enums) {
                    if (key.equals(enumName.getName())) {
                        return 6;
                    }
                }
                Collection<GmlEnumEntryDeclaration> enumEntries = PsiTreeUtil.collectElementsOfType(file, GmlEnumEntryDeclaration.class);
                for (GmlEnumEntryDeclaration enumEntry : enumEntries) {
                    if (key.equals(enumEntry.getIdentifier().getText())) {
                        return 7;
                    }
                }

                if (fileName.equals(file.getName())) {
                    GmlEventDeclaration parent = PsiTreeUtil.getParentOfType(el, GmlEventDeclaration.class);
                    if (parent != null) {
                        Collection<GmlVarDeclaration> pp = PsiTreeUtil.collectElementsOfType(parent, GmlVarDeclaration.class);
                        for (GmlVarDeclaration property : pp) {
                            if (key.equals(property.getName())) {
                                if (property.isLocal()) {
                                    return 1;
                                }
                            }
                        }
                    }
                }

                if (file.getName().equals(String.format("%s.gmo", key))) {
                    Collection<GmlGmlScript> scriptKey = PsiTreeUtil.collectElementsOfType(file, GmlGmlScript.class);
                    if (!scriptKey.isEmpty()) {
                        return 4; // script
                    }

                    return 5; // objects
                }
            }
        }

        return result;
    }

    public static boolean isUnique(@NotNull GmlEventDeclaration event) {
        String name = event.getName();
        int offset = event.getTextRange().getStartOffset();

        Collection<GmlEventDeclaration> events = PsiTreeUtil.collectElementsOfType(event.getParent(), GmlEventDeclaration.class);
        for (GmlEventDeclaration decl : events) {
            if (decl.getName().equals(name) && decl.getTextRange().getStartOffset() < offset) {
                return false;
            }
        }

        return true;
    }

}