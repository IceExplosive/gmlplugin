package gmlplugin.psi.completions;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiIdentifier;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiWhiteSpace;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.indexing.FileBasedIndex;
import gmlplugin.Files.GmlFileType;
import gmlplugin.Files.GmlTokenTypeSet;
import gmlplugin.GmlFile;
import gmlplugin.icons.GmlIcons;
import gmlplugin.psi.*;
import org.codehaus.groovy.ast.ASTNode;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class CommonBuilder {

    public static void addReferences(@NotNull CompletionResultSet result, @NotNull final CompletionParameters parameters) {
//        for (IElementType type : GmlTokenTypeSet.OBJECT_VARIABLES.getTypes()) {
//            result.addElement(PrioritizedLookupElement.withPriority(
//                    LookupElementBuilder.create(type.toString())
//                            .withTypeText("built-in")
//                            .withIcon(GmlIcons.OBJECT_VAR),
//                    50
//                    )
//            );
//        }
//        for (IElementType type : GmlTokenTypeSet.DEF_CONSTS.getTypes()) {
//            result.addElement(PrioritizedLookupElement.withPriority(LookupElementBuilder.create(type.toString()), 50));
//        }

        Collection<VirtualFile> virtualFiles =
                FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, GmlFileType.INSTANCE,
                        GlobalSearchScope.allScope(parameters.getPosition().getProject()));

        // Soubory
        for (VirtualFile virtualFile : virtualFiles) {
            GmlFile file = (GmlFile) PsiManager.getInstance(parameters.getPosition().getProject()).findFile(virtualFile);
            if (file != null) {
                PsiElement child = file.getFirstChild();
                while (child instanceof PsiWhiteSpace) {
                    child = child.getNextSibling();
                }

                if (child instanceof GmlGmlFunctions) {
                    addFunctions(result, file);
                    continue;
                }

                if (child instanceof GmlGmlInstanceVariables) {
                    addInstanceVariables(result, file);
                    continue;
                }

                if (child instanceof GmlGmlBuiltinConstants) {
                    addBuiltinConstants(result, file);
                    continue;
                }

                if (child instanceof GmlGmlResources) {
                    addResources(result, file);
                    continue;
                }

                boolean isObject = !(child instanceof GmlGmlScript);
                // Object / script
                if (isObject) {
                    result.addElement(PrioritizedLookupElement.withPriority(
                            LookupElementBuilder.create(file.getName().substring(0, file.getName().lastIndexOf(".")))
                                    .withTypeText("Object")
                                    .withIcon(GmlIcons.OBJECT),
                            20
                            )
                    );
                } else {
                    addScript(result, file);
                }

                // Konstanty
                Collection<GmlConstDeclaration> properties = PsiTreeUtil.collectElementsOfType(file, GmlConstDeclaration.class);
                for (GmlConstDeclaration gmlConst : properties) {
                    result.addElement(PrioritizedLookupElement.withPriority(
                            LookupElementBuilder.create(gmlConst.getConstName())
                                    .withTypeText("constant")
                                    .withIcon(GmlIcons.CONST),
                            1
                            )
                    );
                }

                // Globální
                Collection<GmlGlobalvarDeclaration> globals = PsiTreeUtil.collectElementsOfType(file, GmlGlobalvarDeclaration.class);
                for (GmlGlobalvarDeclaration gmlglobal : globals) {
                    result.addElement(PrioritizedLookupElement.withPriority(
                            LookupElementBuilder.create(gmlglobal.getGlobalName())
                                    .withTypeText("global")
                                    .withIcon(GmlIcons.GLOBAL_VAR),
                            1
                            )
                    );
                }

                // Vlastní soubor
                if (parameters.getOriginalFile().getName().equals(file.getName())) {
                    Collection<GmlVarDeclaration> objVars = PsiTreeUtil.collectElementsOfType(file, GmlVarDeclaration.class);

                    GmlEventDeclaration event = PsiTreeUtil.getParentOfType(parameters.getPosition(), GmlEventDeclaration.class);
                    String eventName = event != null ? event.getName() : null;

                    // Lokální proměnné
                    for (GmlVarDeclaration var : objVars) {
                        if (var.isLocal() && eventName != null) {
                            GmlEventDeclaration ev = PsiTreeUtil.getParentOfType(var, GmlEventDeclaration.class);
                            if (ev != null && !eventName.equals(ev.getName())) {
                                continue;
                            }
                        }
                        result.addElement(PrioritizedLookupElement.withPriority(
                                LookupElementBuilder.create(var.getName())
                                        .withTypeText(var.isLocal() ? "local" : "variable")
                                        .withIcon(var.isLocal() ? GmlIcons.LOCAL : GmlIcons.VARIABLE),
                                var.isLocal() ? 100 : 80
                                )
                        );
                    }
                }
            }
        }

        result.stopHere();
    }

    private static void addFunctions(@NotNull CompletionResultSet result, @NotNull GmlFile file) {
        Collection<GmlFunctionDeclaration> properties = PsiTreeUtil.collectElementsOfType(file, GmlFunctionDeclaration.class);
        for (GmlFunctionDeclaration decl : properties) {
            StringBuilder name = new StringBuilder();
            name.append(decl.getFunctionDeclarationName().getText());
            name.append("(");
            name.append(decl.getFunctionArgListDecl().getText());
            name.append(")");
// TODO name
            result.addElement(PrioritizedLookupElement.withPriority(
                    LookupElementBuilder.create(decl.getFunctionDeclarationName().getText() + "();")
                            .withTypeText(decl.getFuncReturnval().getFuncReturnName().getText())
                            .withInsertHandler(GmlInsertHandler.INSTANCE)
                            .withPresentableText(name.toString())
                            .withIcon(GmlIcons.SCRIPT),
                    55
                    )
            );
        }
    }

    private static void addResources(@NotNull CompletionResultSet result, @NotNull GmlFile file) {
        PsiTreeUtil.findChildrenOfType(file, GmlGmlResource.class).forEach(i -> {
            String name = i.getResourceName();
            result.addElement(PrioritizedLookupElement.withPriority(
                    LookupElementBuilder.create(name)
                            .withTypeText("resource")
                            .withPresentableText(name)
                            .withIcon(GmlIcons.OBJECT),
                    30
                    )
            );
        });
    }

    private static void addInstanceVariables(@NotNull CompletionResultSet result, @NotNull GmlFile file) {
        PsiTreeUtil.findChildrenOfType(file, GmlGmlInstanceVariable.class).forEach(i -> {
            result.addElement(PrioritizedLookupElement.withPriority(
                    LookupElementBuilder.create(i.getName())
                            .withTypeText("built-in")
                            .withIcon(GmlIcons.OBJECT_VAR),
                    50
                    )
            );
        });
    }

    private static void addBuiltinConstants(@NotNull CompletionResultSet result, @NotNull GmlFile file) {
        PsiTreeUtil.findChildrenOfType(file, GmlGmlBuiltinConstant.class).forEach(i -> {
            result.addElement(PrioritizedLookupElement.withPriority(
                    LookupElementBuilder.create(i.getName()),
                    50
                    )
            );
        });
    }

    private static void addScript(@NotNull CompletionResultSet result, @NotNull GmlFile file) {
        Collection<GmlScriptFunctionDeclaration> scripts = PsiTreeUtil.findChildrenOfType(file, GmlScriptFunctionDeclaration.class);

        for (GmlScriptFunctionDeclaration script : scripts) {
            String name = script.getIdentifier().getText();
            result.addElement(PrioritizedLookupElement.withPriority(
                    LookupElementBuilder.create(String.format("%s();", name))
                            .withInsertHandler(GmlInsertHandler.INSTANCE)
                            .withTypeText("Script")
                            .withPresentableText(name)
                            .withIcon(GmlIcons.SCRIPT),
                    20
                    )
            );
        }
    }

}
