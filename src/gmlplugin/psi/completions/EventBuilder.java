package gmlplugin.psi.completions;

import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.tree.IElementType;
import gmlplugin.Files.GmlTokenTypeSet;
import org.jetbrains.annotations.NotNull;

public class EventBuilder {

    public static void addEventTypes(@NotNull CompletionResultSet result) {
        for (IElementType type : GmlTokenTypeSet.EVENT_TYPES.getTypes()) {
            result.addElement(LookupElementBuilder.create(type.toString()));
        }
    }

    public static void addEvent(@NotNull CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create("event"));
    }

}
