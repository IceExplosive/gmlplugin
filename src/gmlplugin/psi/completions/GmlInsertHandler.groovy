package gmlplugin.psi.completions

import com.intellij.codeInsight.completion.InsertHandler
import com.intellij.codeInsight.completion.InsertionContext
import com.intellij.codeInsight.lookup.LookupElement

public class GmlInsertHandler implements InsertHandler<LookupElement> {
    public static final GmlInsertHandler INSTANCE = new GmlInsertHandler();

    @Override
    void handleInsert(InsertionContext context, LookupElement element) {
        context.getEditor().getCaretModel().moveCaretRelatively(-2, 0, false, false, false);
    }
}
