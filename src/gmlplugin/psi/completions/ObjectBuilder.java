package gmlplugin.psi.completions;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.completion.PrioritizedLookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiWhiteSpace;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import gmlplugin.Files.GmlFileType;
import gmlplugin.GmlFile;
import gmlplugin.icons.GmlIcons;
import gmlplugin.psi.GmlGmlFunctions;
import gmlplugin.psi.GmlGmlResources;
import gmlplugin.psi.GmlGmlScript;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public class ObjectBuilder {

    public static void addObjectNames(@NotNull CompletionResultSet result, @NotNull final CompletionParameters parameters) {
        Collection<VirtualFile> virtualFiles =
                FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, GmlFileType.INSTANCE,
                        GlobalSearchScope.allScope(parameters.getPosition().getProject()));
        // Soubory
        for (VirtualFile virtualFile : virtualFiles) {
            GmlFile file = (GmlFile) PsiManager.getInstance(parameters.getPosition().getProject()).findFile(virtualFile);
            if (file != null) {
                PsiElement child = file.getFirstChild();
                while (child instanceof PsiWhiteSpace) {
                    child = child.getNextSibling();
                }

                if (!(child instanceof GmlGmlScript)) {
                    String filename = file.getName();
                    if (filename.equals("_functionList.gmo")) {
                        continue;
                    }
                    if (filename.equals("_resources.gmo")) {
                        continue;
                    }

                    result.addElement(PrioritizedLookupElement.withPriority(
                            LookupElementBuilder.create(filename.substring(0, file.getName().lastIndexOf(".")))
                                    .withTypeText("Object")
                                    .withIcon(GmlIcons.OBJECT),
                            20
                            )
                    );
                }
            }
        }
    }

}
