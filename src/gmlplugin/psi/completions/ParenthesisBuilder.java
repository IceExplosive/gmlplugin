package gmlplugin.psi.completions;

import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import org.jetbrains.annotations.NotNull;

public class ParenthesisBuilder {

    public static void addRound(@NotNull CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create("(\n)"));
    }

    public static void addCurl(@NotNull CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create("{\n}"));
    }

}
