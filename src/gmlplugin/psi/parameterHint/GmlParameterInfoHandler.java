package gmlplugin.psi.parameterHint;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.lang.parameterInfo.*;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import gmlplugin.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class GmlParameterInfoHandler implements ParameterInfoHandler<GmlScriptCall, GmlFunctionArgListDecl> {

    @Nullable
    @Override
    public GmlScriptCall findElementForParameterInfo(@NotNull CreateParameterInfoContext context) {
        PsiElement element = context.getFile().findElementAt(context.getOffset());
        GmlScriptCall call = PsiTreeUtil.getParentOfType(element, GmlScriptCall.class);

        if (call != null) {
            GmlFunctionArgListDecl list = GmlUtils.getFuncArgList(call);
            context.setItemsToShow(new GmlFunctionArgListDecl[] { list });
        }

        return call;
    }

    @Override
    public void updateUI(GmlFunctionArgListDecl list, @NotNull ParameterInfoUIContext context) {
        List<GmlFuncArgDecl> args = list.getFuncArgDeclList();
        int index = context.getCurrentParameterIndex();
        int limit = args.size();

        GmlFuncArgDecl param = (index >= 0) ? args.get(index < limit ? index : limit - 1) : null;
        int start = -1, end = -1;
        if (param != null) {
            start = param.getTextRange().getStartOffset() - list.getTextOffset();
            end = param.getTextRange().getEndOffset() - list.getTextOffset();
        }

        context.setupUIComponentPresentation(list.getText(), start, end, false, false, false, context.getDefaultParameterColor());
    }

    @Override
    public void showParameterInfo(@NotNull GmlScriptCall element, @NotNull CreateParameterInfoContext context) {
        context.showHint(element, element.getTextRange().getStartOffset(), this);
    }

    @Nullable
    @Override
    public GmlScriptCall findElementForUpdatingParameterInfo(@NotNull UpdateParameterInfoContext context) {
        // TODO při invaliditě zmizí hint
        PsiElement element = context.getFile().findElementAt(context.getOffset());
        if (element == null) {
            return null;
        }

        GmlScriptCall call = PsiTreeUtil.getParentOfType(element, GmlScriptCall.class);
        if (call == null) {
            return null;
        }

        int index = 0;
        if (call.getFunctionArgList() != null) {
            index = ParameterInfoUtils.getCurrentParameterIndex(call.getFunctionArgList().getNode(), context.getOffset(), GmlTypes.COMMA);
        }
        context.setCurrentParameter(index);

        return call;
    }

    @Override
    public void updateParameterInfo(@NotNull GmlScriptCall element, @NotNull UpdateParameterInfoContext context) {
    }
    @Override
    public boolean couldShowInLookup() {
        return true;
    }

    @Nullable
    @Override
    public Object[] getParametersForLookup(LookupElement item, ParameterInfoContext context) {
        return null;
    }

}
