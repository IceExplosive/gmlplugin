package gmlplugin.psi.references;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface GmlNamedElement extends PsiNameIdentifierOwner {
}
