package gmlplugin.psi.references;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import org.jetbrains.annotations.NotNull;

public abstract class GmlNamedElementImpl extends ASTWrapperPsiElement implements GmlNamedElement {

    public GmlNamedElementImpl(@NotNull ASTNode node) {
        super(node);
    }

}
